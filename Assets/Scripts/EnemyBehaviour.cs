﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public float stunDuration = 1f;
    public GameObject deathParticals;
	//public GameObject deathSounds;

    private Animator animator;
    private EnemyGroundControl enemyMovement;
    private bool isDead = false;

    // Use this for initialization
    void Start() {
        animator = GetComponent<Animator>();
        enemyMovement = transform.parent.GetComponent<EnemyGroundControl>();
    }

    public void StunEnemy() {
        StartCoroutine(AnimationManager.AnimateEnemyStun(animator, stunDuration));
        StartCoroutine(StopEnemyMovement(stunDuration));
    }

    private IEnumerator StopEnemyMovement(float duration) {
        enemyMovement.canMove = false;
        yield return new WaitForSeconds(duration);
        if (!isDead) {
            enemyMovement.canMove = true;
        }
    }

    public void Die()
    {
        //AnimationManager.AnimateEnemyDeath(animator);
        isDead = true;
        enemyMovement.canMove = false;

        // Not the best way to disable it but works
        transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
		//Instantiate (deathSounds, transform.position, Quaternion.identity);
        Instantiate(deathParticals, transform.position, deathParticals.transform.rotation);
        Destroy(gameObject);
        Destroy(transform.parent.gameObject);

        //StartCoroutine(Die(2f));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            Die();
        }
    }

    private IEnumerator Die(float waitDuration) {
        yield return new WaitForSeconds(waitDuration);
        Destroy(gameObject);
        Destroy(transform.parent.gameObject);
    }

    
}