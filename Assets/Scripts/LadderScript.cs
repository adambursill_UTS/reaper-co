﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class LadderScript : MonoBehaviour {

    private GameObject player;
    public float climbSpeed = 0;    

    private PlayerMovement movement;

    public static bool onLadder = false;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
        movement = GameObject.Find("Player").GetComponent<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
		
        if (onLadder == true && Input.GetKey(KeyCode.W) || onLadder == true && Input.GetAxisRaw("Vertical") == 1)
        {
            player.transform.position += Vector3.up / climbSpeed;
        }
        if (onLadder == true && Input.GetKey(KeyCode.S) || onLadder == true && Input.GetAxisRaw("Vertical") == -1)
        {
            player.transform.position += Vector3.down / climbSpeed;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            onLadder = true;
            PlayerMovement.gravity = 0f;
            movement.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            onLadder = false;
            PlayerMovement.gravity = 20f;
            movement.enabled = true;
        }
    }
}
