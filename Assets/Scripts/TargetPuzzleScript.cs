﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPuzzleScript : MonoBehaviour {

	//private MeshCollider meshCollider;
	public static int targetsHit = 0;
	private bool isHit = false;

	// Use this for initialization
	void Start () {
		//meshCollider = GetComponent<MeshCollider> ();
		
	}
	
	// Update is called once per frame
	void OnCollisionEnter(Collision col) {
		Collider other = col.collider;

		if (other.tag == "Bone" && isHit == false) {
			targetsHit++;
			isHit = true;
            if(GetComponent<Animator>() != null) {
                GetComponent<Animator>().SetTrigger("TargetHit");
            }
			Debug.Log (targetsHit);
			//meshCollider.enabled = false;
		}
	}
}
