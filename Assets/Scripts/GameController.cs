﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

	private Health health;
	public int maxHealth;
    public AudioClip[] playerDamageSounds;
    private AudioSource playerBodyAudioSource;
    private int playerDamageIterator = 0;
	public int lives;
	public static bool isDead = false;
	//private bool deathDialogue = false;
	private bool shame = false;

	private bool invincibilityFrame = false;

	public static bool miniGunBool;
	public static bool bonerangBool;
	public static bool magicWandBool;

    private UIManager uIManager;

	private GameObject player;

	public Transform Checkpoint { get; set; }

	public bool LoadGame;
    public GameObject followCMCamera;
    public GameObject loadingScreen;

    private Inventory playerInventory;
    private PlayerCostumeChange playerCostume;
    private PlayerAbilities playerAbilities;
    private CameraSettlingChange cameraSettling;
    private DialogueManager dialogueManager;
	private GameObject boss;
    public bool InReapingCutscene { get; set; }

    private void Awake()
    { 
        player = GameObject.Find("Player");
        cameraSettling = GameObject.Find("CM FreeLook follow").GetComponent<CameraSettlingChange>();
        if (LoadGame)
        {
            InputManager.PlayerInputsDisabled = true;
            InputManager.CameraInputsDisabled = true;

            StartCoroutine(LoadLevelHubWorld());
        }
    }

    // Use this for initialization
    void Start ()
    {
        isDead = false;
        miniGunBool = false;
        bonerangBool = true;
        magicWandBool = false;

        health = GetComponent<Health> ();
        uIManager = GetComponent<UIManager>();
		Checkpoint = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ().transform;
        health.setHealth (maxHealth);
        playerBodyAudioSource = GetComponent<AudioSource>();
        //playerDamage[playerDamageIterator].

        playerInventory = player.GetComponent<Inventory>();
        playerCostume = player.GetComponent<PlayerCostumeChange>();
        playerAbilities = player.GetComponent<PlayerAbilities>();
        dialogueManager = GameObject.Find("DialogueManager").GetComponent<DialogueManager>();
        SetBonerang();
		//SetMagicWand();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Debug.Log(InputManager.Instance.GetCameraInputAxis("Camera X"));
		if (InputManager.Respawn == true) {

            boss = GameObject.Find("Boss");
            if (boss != null)
            {
                boss.GetComponent<BossController>().ResetBossPhase();
            }
            //GameObject.Find ("skeleton_mesh").GetComponent<SkinnedMeshRenderer> ().enabled = true;
            isDead = false;
			player.transform.position = Checkpoint.position;
			player.transform.rotation = Checkpoint.rotation;
            Checkpoint.GetComponent<Checkpoint>().SpawnParticles();
            playerAbilities.StartTimeRewindCoolDown();
			//Checkpoint.GetChild (0).GetChild (0).GetComponent<ParticleSystem> ().Play ();
			//Checkpoint.GetChild (0).GetChild (1).GetComponent<ParticleSystem> ().Play ();
			//Checkpoint.GetChild (0).GetComponent<ParticleSystem> ().Play ();
			//Checkpoint.GetComponentInChildren<ParticleSystem> ().Play ();
			health.setHealth (maxHealth);
			//deathDialogue = false;
			lives--;
			uIManager.UpdateLivesRemainingUI (lives);
			InputManager.Respawn = false;
			AnimationManager.AnimatePlayerDeath (false);
            if (lives == 0 && shame == false)
            {
                dialogueManager.StartShameDialog();
                shame = true;
            }
        }

        
        //InputManager.Instance.GetCameraInputAxis("Camera X");


        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            StartCoroutine(GameShortCut(1));
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StartCoroutine(GameShortCut(2));
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StartCoroutine(GameShortCut(3));
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            StartCoroutine(GameShortCut(4));
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            //cameraSettling.SetBossCameraSettings();
            StartCoroutine(GameShortCut(5));
        }

    }

    private IEnumerator LoadLevelHubWorld()
    {
        loadingScreen.SetActive(true);
        SceneManager.LoadScene(2, LoadSceneMode.Additive);
        SceneManager.LoadScene(3, LoadSceneMode.Additive);

        yield return new WaitForSeconds(0.2f);


        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(3));

        //// Wait until the asynchronous scene fully loads
        //while (!asyncLoad.isDone)
        //{
        //    yield return null;
        //}

        player.transform.position = new Vector3(40.5f, 0.66f, 4.28f);
        player.transform.localRotation = Quaternion.Euler(0f, -180f, 0f);
        yield return new WaitForSeconds(0.2f);
        loadingScreen.SetActive(false);

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;
    }

	private IEnumerator InvincibilityFrames() {
		invincibilityFrame = true;
        /*List<GameObject> costumes = player.GetComponent<PlayerCostumeChange>().GetCurrentCostume();
        foreach (GameObject costume in costumes) {
            if (costume.GetComponent<Renderer>() != null) {
                costume.GetComponent<Renderer>().enabled = false;
            }
        }
        yield return new WaitForSeconds(0.5f/3);
        foreach (GameObject costume in costumes) {
            if (costume.GetComponent<Renderer>() != null) {
                costume.GetComponent<Renderer>().enabled = true;
            }
        }
        yield return new WaitForSeconds(0.5f / 3);
        foreach (GameObject costume in costumes) {
            if (costume.GetComponent<Renderer>() != null) {
                costume.GetComponent<Renderer>().enabled = true;
            }
        }

        yield return new WaitForSeconds(0.5f / 3);
        foreach (GameObject costume in costumes) {
            if (costume.GetComponent<Renderer>() != null) {
                costume.GetComponent<Renderer>().enabled = true;
            }
        }*/
        yield return new WaitForSeconds(0.5f);
        invincibilityFrame = false;
	}

    public void LoadBossLevel()
    {
        cameraSettling.SetBossCameraSettings();
        StartCoroutine(LoadLevelBossLevelFast());
    }

    private IEnumerator LoadLevelBossLevelFast()
    {
        playerInventory.ClearInventory();
        InputManager.PlayerInputsDisabled = true;
        InputManager.CameraInputsDisabled = true;

        loadingScreen.SetActive(true);

        SceneManager.LoadScene(4, LoadSceneMode.Additive);
        //AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(1);

        yield return new WaitForSeconds(0.1f);

        // Wait until the asynchronous scene fully loads
        //while (!asyncLoad.isDone /*|| !asyncUnload.isDone*/)
        //{
        //    yield return null;
        //}

        yield return new WaitForSeconds(1f);

        player.transform.position = new Vector3(-356.2f, 53.11f, -255.95f);
        player.transform.localRotation = Quaternion.Euler(0f, -181.58f, 0f);

        cameraSettling.SetBossCameraSettings();

        yield return new WaitForSeconds(2f);
        
        loadingScreen.SetActive(false);
        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;
    }

    public void LoadBossLevelAsync()
    {
        InReapingCutscene = true;
        StartCoroutine(LoadLevelBossLevelAsync());
    }

    private IEnumerator LoadLevelBossLevelAsync()
    {
        playerInventory.ClearInventory();
        yield return new WaitForSeconds(1f);

        InputManager.PlayerInputsDisabled = true;
        InputManager.CameraInputsDisabled = true;

        AsyncOperation loadBossLevel = SceneManager.LoadSceneAsync(4, LoadSceneMode.Additive);

        //Wait until the asynchronous scene fully loads
        while (!loadBossLevel.isDone)
        {
            yield return null;
        }

        yield return new WaitForSeconds(6.5f);      

        player.transform.position = new Vector3(-356.2f, 53.11f, -255.95f);
        player.transform.localRotation = Quaternion.Euler(0f, -181.58f, 0f);

        cameraSettling.SetBossCameraSettings();

        while (InReapingCutscene)
        {
            yield return null;
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(4));

        GameObject.Find("BossEvents").GetComponent<BossEvents>().ActivateDeathIntro();
        GameObject.Find("BossEvents").GetComponent<BossEvents>().PlayDutchRollTimeline();

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;

        SceneManager.MoveGameObjectToScene(player, SceneManager.GetSceneByBuildIndex(1));

        yield return new WaitForSeconds(1f);

        try
        {
            SceneManager.UnloadSceneAsync(3);
            SceneManager.UnloadSceneAsync(2);
        }
        catch (Exception ex)
        {
            print(ex.Message);
        }

    }

    public void PauseGame()
    {
        uIManager.PauseMenu();
    }

    public void GameWon()
    {
        float fadeToCredits = 5f;

        uIManager.FadeOutScreen(fadeToCredits);
        StartCoroutine(gameFinished(fadeToCredits));
    }

    private IEnumerator gameFinished(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        PlayerPrefs.SetInt("Credits?", 1);
        SceneManager.LoadScene(0);
    }  

	public void UpdateCheckpoint(Transform check) {
		Checkpoint = check;
	}

	public Transform UseCheckPoint() {
		return Checkpoint;
	}

	public void loseHealth(int lose)
    {
		if (isDead == false)
        {
			if (invincibilityFrame == false && !playerAbilities.TimeRewinding)
            {
				StartCoroutine (InvincibilityFrames());
                int tempHealth = health.getHealth ();
				tempHealth -= lose;
				if (tempHealth > 0)
                {
					health.setHealth (tempHealth);
				}
                else
                {
                    isDead = true;
                    boss = GameObject.Find("Boss");
                    if (boss != null)
                    {
                        boss.GetComponent<BossController>().BossPlayerDied();
                    }
                    playerAbilities.RemovePlayerWand();
                    health.setHealth (tempHealth);
                    AnimationManager.AnimatePlayerDeath(true);
                    dialogueManager.StartDeathDialog();
                    //StartCoroutine (dying ());
                }
				playerBodyAudioSource.PlayOneShot (playerDamageSounds [playerDamageIterator]);
				if (playerDamageIterator == playerDamageSounds.Length - 1)
                {
					playerDamageIterator = 0;
				}
                else
                {
					++playerDamageIterator;
				}
                player.GetComponent<ImpactReceiver>().SpawnDamageParticles();
            }
		}
    }

    public IEnumerator GameShortCut(int number)
    {
        AsyncOperation hubAsyncUnLoad = new AsyncOperation();
        AsyncOperation medAsyncUnLoad = new AsyncOperation();
        AsyncOperation bossAsyncUnLoad = new AsyncOperation();

        InputManager.PlayerInputsDisabled = true;
        InputManager.CameraInputsDisabled = true;
		Checkpoint = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ().transform;

        loadingScreen.SetActive(true);

        SceneManager.MoveGameObjectToScene(player, SceneManager.GetSceneByBuildIndex(1));

        yield return new WaitForSeconds(0.5f);

        try
        {
            hubAsyncUnLoad = SceneManager.UnloadSceneAsync(2);
        }
        catch (Exception ex)
        {
            print(ex.Message);
        }

        yield return hubAsyncUnLoad;

        try
        {
            medAsyncUnLoad = SceneManager.UnloadSceneAsync(3);
        }
        catch (Exception ex)
        {
            print(ex.Message);
        }

        yield return medAsyncUnLoad;

        try
        {
            bossAsyncUnLoad = SceneManager.UnloadSceneAsync(4);
        }
        catch (Exception ex)
        {
            print(ex.Message);
        }

        yield return bossAsyncUnLoad;

        playerInventory.ClearInventory();
        playerAbilities.disableMiniGun();
        playerCostume.WearPrincessHat(false);
        playerCostume.CostumeChange(PlayerCostumeChange.Costume.Default);

        switch (number)
        {
            case 1:
                SceneManager.LoadScene(2, LoadSceneMode.Additive);
                SceneManager.LoadScene(3, LoadSceneMode.Additive);
                yield return null;

                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(3));
                player.transform.position = new Vector3(56.32f, 0.66f, -164.79f);
                player.transform.localRotation = Quaternion.Euler(0f, -180f, 0f);

                cameraSettling.RestCamera();
                break;

            case 2:
                SceneManager.LoadScene(2, LoadSceneMode.Additive);
                SceneManager.LoadScene(3, LoadSceneMode.Additive);
                yield return null;

                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(3));
                player.transform.position = new Vector3(37.56187f, 37.11f, -210.4417f);
                player.transform.localRotation = Quaternion.Euler(0f, 95.32f, 0f);

                cameraSettling.RestCamera();
                break;

            case 3:
                SceneManager.LoadScene(3, LoadSceneMode.Additive);
                yield return null;

                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(3));
                player.transform.position = new Vector3(39.6703f, 0.4156179f, -278.4117f);
                player.transform.localRotation = Quaternion.Euler(0f, -179.213f, 0f);

                cameraSettling.RestCamera();
                break;

            case 4:
                SceneManager.LoadScene(3, LoadSceneMode.Additive);
                yield return null;

                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(3));

                playerCostume.WearPrincessHat(true);
                playerCostume.CostumeChange(PlayerCostumeChange.Costume.PrincessDress);

                player.transform.position = new Vector3(36.47f, 7.097651f, -396.78f);
                player.transform.localRotation = Quaternion.Euler(0f, 177.683f, 0f);

                cameraSettling.RestCamera();
                break;

            case 5:
                SceneManager.LoadScene(4, LoadSceneMode.Additive);
                yield return null;

                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(4));

                GameObject.Find("BossEvents").GetComponent<BossEvents>().ActivateDeathIntro();

                playerCostume.WearPrincessHat(true);
                playerCostume.CostumeChange(PlayerCostumeChange.Costume.PrincessDress);

                player.transform.position = new Vector3(-356.2f, 50.11f, -255.95f);
                player.transform.localRotation = Quaternion.Euler(0f, -181.58f, 0f);


                cameraSettling.SetBossCameraSettings();
                break;
        }
        yield return new WaitForSeconds(2f);

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;

        loadingScreen.SetActive(false);
    }


	public int getMaxHealth()
	{
		return maxHealth;
	}

	public int getCurrentHealth() {
		return health.getHealth ();
	}

    void ClearUI()
    {
        uIManager.GameWonPanel.SetActive(false);
    }

	public void SetMiniGun() {
		miniGunBool = true;
		bonerangBool = false;
		magicWandBool = false;
	}

	public void SetBonerang() {
		miniGunBool = false;
		bonerangBool = true;
		magicWandBool = false;
        playerAbilities.RemovePlayerWand();
    }

	public void SetMagicWand() {
		miniGunBool = false;
		bonerangBool = false;
		magicWandBool = true;
        playerAbilities.GivePlayerWand();
	}

}
