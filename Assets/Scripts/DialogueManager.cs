﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class DialogueManager : MonoBehaviour
{

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;

    public DialogueTrigger DeathBox;
    public DialogueTrigger ShameBox;

    private Queue<string> sentences;
    private UIManager uIManager;

    private UnityEvent dialogFinishEvent;

    private DialogueTrigger dialogTrig;

    private bool displayingSentence = false;
    private string currentSentence;
    private InputManager inputManager;

    // Use this for initialization
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        uIManager = gameControllerObject.GetComponent<UIManager>();
        sentences = new Queue<string>();
        inputManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InputManager>();
    }
    void Update()
    {

    }

    public void StartShameDialog()
    {
        StartDialogue(ShameBox.dialogues[0], ShameBox.Name);
    }

    public void StartDeathDialog()
    {
        Dialogue dialogue = DeathBox.dialogues[0];
        UIManager.inDialogue = true;
        uIManager.dialoguePanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        dialogFinishEvent = dialogue.DialogFinished;


        nameText.text = DeathBox.Name;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        currentSentence = sentences.Dequeue();
        CheckStringForSprites();
        StopAllCoroutines();
        dialogueText.text = currentSentence;
    }

    public void StartDialogue(Dialogue dialogue, string name)
    {
        UIManager.inDialogue = true;
        uIManager.dialoguePanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        dialogFinishEvent = dialogue.DialogFinished;


        nameText.text = name;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences) {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void StartDialogue(Dialogue dialogue, string name, DialogueTrigger dialogueTrigger) {
        UIManager.inDialogue = true;
        uIManager.dialoguePanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        dialogTrig = dialogueTrigger;


        dialogFinishEvent = dialogue.DialogFinished;


        nameText.text = name;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences) {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        //Debug.Log("displaynext");
        if (displayingSentence)
        {
            StopAllCoroutines();
            dialogueText.text = currentSentence;
            displayingSentence = false;
            return;
        }

        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        currentSentence = sentences.Dequeue();
        CheckStringForSprites();
        StopAllCoroutines();
        StartCoroutine(TypeSentence());
    }

    public void DisplayNextSentenceAfterDeath()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        currentSentence = sentences.Dequeue();
        CheckStringForSprites();
        StopAllCoroutines();
        StartCoroutine(TypeSentence());
    }

    IEnumerator TypeSentence()
    {
        bool spriteFormat = false;
        displayingSentence = true;
        dialogueText.text = "";
        foreach (char letter in currentSentence.ToCharArray())
        {
            if(letter == '<')
            {
                spriteFormat = true;
            }
            if (letter == '>')
            {
                spriteFormat = false;
            }

            dialogueText.text += letter;
            if(!spriteFormat)
            {
                yield return null;
            }
            
        }

        yield return new WaitForSeconds(0.25f);
        displayingSentence = false;
    }

    public void EndDialogue() {
        if (dialogTrig != null)
        {
            dialogTrig.TalkingAnimation(false);
            dialogTrig.ReturnToOriginalLook();
            dialogTrig = null;
        }
        uIManager.dialoguePanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        dialogFinishEvent.Invoke();

        StartCoroutine(noMore());
    }
    IEnumerator noMore() {
        yield return new WaitForSeconds(0.1f);
        UIManager.inDialogue = false;
    }

    public void DisplayNextIfNotDead() {
        if (!GameController.isDead) {
            DisplayNextSentence();
        }

    }

/* Dialog conversion
$01: X, A, space
$02: O, B, right mouse
$03: Square, X, left mouse
$04: Triangle, Y, E
$05: Left Stick, WASD
$06: Right Stick, mouse
$07: R1, RB, right mouse
$08: L1, LB, Q
$09: L2, LT, R
$10: R2, RT, 
$11: D Pad
*/

    private void CheckStringForSprites()
    {
        string str = currentSentence;
        string spr = "&";

        if (str.Contains("$"))
        {
            var sprsub = str.Substring(str.IndexOf("$"), 3);

            var sub1 = str.Substring(0, str.IndexOf("$"));
            var sub2 = str.Substring(str.IndexOf("$") + 3, str.Length - (str.IndexOf("$") + 3) );

            //Debug.Log(sprsub);
            //Debug.Log(sub1);
            //Debug.Log(sub2);

            //Debug.Log(sub1 + spr +sub2);

            switch (inputManager.GetCurrentInputDevice())
            {
                case InputManager.CurrentInput.Keyboard:
                    switch (sprsub)
                    {
                        case "$01":
                            spr = "<sprite=23>";
                            break;
                        case "$02":
                            spr = "<sprite=20>";
                            break;
                        case "$03":
                            spr = "<sprite=21>";
                            break;
                        case "$04":
                            spr = "E";
                            break;
                        case "$05":
                            spr = "<sprite=19>";
                            break;
                        case "$06":
                            spr = "<sprite=22>";
                            break;
                        case "$07":
                            spr = "<sprite=20>";
                            break;
                        case "$08":
                            spr = "<sprite=25>";
                            break;
                        case "$09":
                            spr = "<sprite=26>";
                            break;
                        case "$10":
                            spr = "<sprite=24>";
                            break;
                        case "$11":
                            spr = "";
                            break;
                    }
                    break;
                case InputManager.CurrentInput.PlayStation:
                    switch (sprsub)
                    {
                        case "$01":
                            spr = "<sprite=0>";
                            break;
                        case "$02":
                            spr = "<sprite=1>";
                            break;
                        case "$03":
                            spr = "<sprite=2>";
                            break;
                        case "$04":
                            spr = "<sprite=3>";
                            break;
                        case "$05":
                            spr = "<sprite=4>";
                            break;
                        case "$06":
                            spr = "<sprite=5>";
                            break;
                        case "$07":
                            spr = "<sprite=6>";
                            break;
                        case "$08":
                            spr = "<sprite=7>";
                            break;
                        case "$09":
                            spr = "<sprite=8>";
                            break;
                        case "$10":
                            spr = "<sprite=9>";
                            break;
                        case "$11":
                            spr = "<sprite=10>";
                            break;
                    }
                    break;
                case InputManager.CurrentInput.Xbox:
                    switch (sprsub)
                    {
                        case "$01":
                            spr = "<sprite=11>";
                            break;
                        case "$02":
                            spr = "<sprite=12>";
                            break;
                        case "$03":
                            spr = "<sprite=14>";
                            break;
                        case "$04":
                            spr = "<sprite=13>";
                            break;
                        case "$05":
                            spr = "<sprite=4>";
                            break;
                        case "$06":
                            spr = "<sprite=5>";
                            break;
                        case "$07":
                            spr = "<sprite=15>";
                            break;
                        case "$08":
                            spr = "<sprite=16>";
                            break;
                        case "$09":
                            spr = "<sprite=17>";
                            break;
                        case "$10":
                            spr = "<sprite=18>";
                            break;
                        case "$11":
                            spr = "<sprite=10>";
                            break;
                    }
                    break;
            }
            currentSentence = sub1 + spr + sub2;
            if(currentSentence.Contains("$"))
            {
                CheckStringForSprites();
            }
        }      
    }
}
