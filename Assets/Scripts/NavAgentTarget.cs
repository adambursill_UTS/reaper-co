﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavAgentTarget : MonoBehaviour
{
    private EnemyGroundControl enemyGroundControl;

	// Use this for initialization
	void Start ()
    {
		enemyGroundControl = transform.parent.GetComponent<EnemyGroundControl>();
        

    }

    private void OnTriggerEnter(Collider other)
    {
        
        enemyGroundControl.RandomizPatrolTarget();
        
    }
}
