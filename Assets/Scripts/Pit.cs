﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pit : MonoBehaviour {
	//private GameObject player;
	//Transform newLocation;
	private GameObject gameController;
	private GameController gameScript;

	void Start() {
		//player = GameObject.Find ("Player");
		gameController = GameObject.FindGameObjectWithTag ("GameController");
		gameScript = gameController.GetComponent<GameController> ();
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			//newLocation = gameScript.UseCheckPoint ();
			gameScript.loseHealth (gameScript.getMaxHealth ());
			//GameObject.Find ("skeleton_mesh").GetComponent<SkinnedMeshRenderer> ().enabled = false;
		}
	}
}
