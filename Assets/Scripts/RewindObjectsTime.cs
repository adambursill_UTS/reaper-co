﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindObjectsTime : MonoBehaviour
{
    private float recordTime;
    private PlayerAbilities playerAbilities;
    private List<PointInTime> pointsInTime;
    private Rigidbody rb;

    // Use this for initialization
    void Start ()
    {
        playerAbilities = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAbilities>();
        pointsInTime = new List<PointInTime>();
        rb = GetComponent<Rigidbody>();
        recordTime = playerAbilities.timeRewindAmount;
    }

    void FixedUpdate()
    {
        if (playerAbilities.TimeRewinding)
        {
            Rewind();
            if(rb != null)
            {
                rb.isKinematic = true;
            }           
        }            
        else
        {
            Record();
            if (rb != null)
            {
                rb.isKinematic = false;
            }
        }
            
    }

    // Rewind time by moving object to preivious position and rotation
    void Rewind()
    {
        if (pointsInTime.Count > 0)
        {
            PointInTime pointInTime = pointsInTime[0];
            transform.position = pointInTime.position;
            transform.rotation = pointInTime.rotation;
            pointsInTime.RemoveAt(0);
        }
        //else
        //{
        //    playerAbilities.TimeRewinding = false;
        //}

    }

    // Record time by storing objects position and rotation
    // Only Up too the last record time amount
    void Record()
    {
        if (pointsInTime.Count > Mathf.Round(recordTime / Time.fixedDeltaTime))
        {
            pointsInTime.RemoveAt(pointsInTime.Count - 1);
        }

        pointsInTime.Insert(0, new PointInTime(transform.position, transform.rotation));
    }


}
