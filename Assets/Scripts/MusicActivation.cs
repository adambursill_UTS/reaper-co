﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicActivation : MonoBehaviour {

	private bool hasBeenActivated = false;

	public AudioClip MedWorld;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (hasBeenActivated == true) {
			if (GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume <= 0.499f) {
				GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume += 0.01f;
			}
			if (GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume >= 0.499f) {
				hasBeenActivated = false;
			}

		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			if (GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume <= 0.10f) {
				GameObject.Find ("Main Camera").GetComponent<AudioSource> ().clip = MedWorld;
				GameObject.Find ("Main Camera").GetComponent<AudioSource> ().Play ();
			}
			hasBeenActivated = true;
		}
	}

	public void playMedWorld() {
		hasBeenActivated = true;
		GameObject.Find ("Main Camera").GetComponent<AudioSource> ().clip = MedWorld;
		GameObject.Find ("Main Camera").GetComponent<AudioSource> ().Play ();
	}

	public void stopBGM() {
		GameObject.Find ("Main Camera").GetComponent<AudioSource> ().Stop();
	}

}
