﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChanger : MonoBehaviour {

	private bool changeMusic = false;
	private bool musicChanged = false;
	private bool deactivate = false;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

		if (changeMusic == true) {
			GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume -= 0.02f;
			if (GameObject.Find("Main Camera").GetComponent<AudioSource>().volume <= 0.10f) {
				musicChanged = true;
				changeMusic = false;
			}

		}

		if (musicChanged == true && !deactivate && !changeMusic) {
			GameObject.Find("Main Camera").GetComponent<AudioSource>().Stop();
			//GameObject.Find("Main Camera").GetComponent<AudioSource>().volume = 0.499f;
			musicChanged = false;
			//deactivate = true;
		}

		//if (deactivate == true) {
			//gameObject.SetActive (false);
		//}
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			changeMusic = true;
		}
	}
}
