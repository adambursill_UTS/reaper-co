﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyBehaviour : MonoBehaviour
{
    public GameObject deathParticals;
    private Animator animator;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            animator.SetTrigger("Surround Attack");
        }
        if(other.tag == "Scythe")
        {
            
            Destroy(gameObject);
            SendMessageUpwards("HasDied");
            Instantiate(deathParticals, transform.position, deathParticals.transform.rotation);
        }
		if (other.tag == "Bullet" || other.tag == "Nuke" || other.tag == "ShockWave")
        {

            Destroy(gameObject);
            SendMessageUpwards("HasDied");
            Instantiate(deathParticals, transform.position, deathParticals.transform.rotation);
        }
    }
}
