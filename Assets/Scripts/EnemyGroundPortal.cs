﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyGroundPortal : MonoBehaviour
{
    public GameObject portalPartical;
    public GameObject bossGroundEnemy;
    public GameObject portalParticalClose;

    public bool selfDestroy = false;
    public bool timelineMovement = false;

    private GameObject enemy;

    // Use this for initialization
    void Start ()
    {
        
	}

    private void OnEnable()
    {
        StartCoroutine(spawnSequence());
    }

    private IEnumerator spawnSequence()
    {
        yield return new WaitForSeconds(0.5f);

        var portal = Instantiate(portalPartical, transform.position, Quaternion.identity);
        enemy = Instantiate(bossGroundEnemy, new Vector3(transform.position.x, transform.position.y - 2, transform.position.z), Quaternion.identity);
        var enemyRB = enemy.GetComponent<Rigidbody>();
        var enemyAgent = enemy.GetComponent<NavMeshAgent>();

        enemyAgent.enabled = false;
        enemyRB.isKinematic = true;
        enemyRB.useGravity = false;

        yield return new WaitForSeconds(1f);

        iTween.MoveTo(enemy, transform.position, 5f);


        yield return new WaitForSeconds(3f);
        enemyAgent.enabled = true;
        enemy.GetComponent<Rigidbody>().isKinematic = false;
        enemyRB.useGravity = true;
        
        if(!timelineMovement)
        {
            enemy.GetComponent<EnemyGroundBossControl>().CanMove = true;
        }
        
        yield return new WaitForSeconds(0.5f);
        iTween.ScaleTo(portal, new Vector3(0.2f, 0.2f, 0.2f), 0.5f);
        yield return new WaitForSeconds(0.5f);

        var portalClose = Instantiate(portalParticalClose, transform.position, portalParticalClose.transform.rotation);
        Destroy(portal);
        Destroy(portalClose, 1.5f);

        if(selfDestroy)
        {
            Destroy(gameObject);
        }       
    }

    private void OnDisable()
    {
        if(enemy != null)
        {
            enemy.GetComponent<EnemyGroundBossControl>().CanMove = true;
        }
        
    }
}
