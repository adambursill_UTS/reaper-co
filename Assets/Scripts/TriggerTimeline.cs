﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Cinemachine.Timeline;

public class TriggerTimeline : MonoBehaviour
{
    public List<Animator> animators;
    public List<RuntimeAnimatorController> animatorControllers;

	private new AudioSource audio;

    private PlayableDirector timeline;


	// Use this for initialization
	void Start ()
    {
		audio = GetComponent<AudioSource> ();
        timeline = GetComponent<PlayableDirector>();
		GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume = 0.499f;

        var track = (timeline.playableAsset as TimelineAsset).GetOutputTrack(3);
        timeline.SetGenericBinding(track, GameObject.FindGameObjectWithTag("MainCamera"));      

    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    private void OnTriggerEnter(Collider other)
    {
		
        if(other.tag == "Player")
        {
            startTimeline();
        }
    }

    private void startTimeline()
    {
        audio.Play();
        foreach (var a in animators)
        {
            a.runtimeAnimatorController = null;
        }
        GameObject.Find("Main Camera").GetComponent<AudioSource>().Stop();
        timeline.Play();
        StartCoroutine(enableControlsAfterTimeline(timeline.duration));
    }


    private void OnDisable()
    {
        if(animators != null)
        {
            for (int i = 0; i < animators.Count; i++)
            {
                if (animators[i] != null && animatorControllers[i] != null)
                {
                    animators[i].runtimeAnimatorController = animatorControllers[i];
                }               
            }
        }

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;
        //GameObject.Find ("Main Camera").GetComponent<AudioSource> ().Play();
    }

    private IEnumerator enableControlsAfterTimeline(double waitTime)
    {
        InputManager.PlayerInputsDisabled = true;
        InputManager.CameraInputsDisabled = true;

        yield return new WaitForSeconds((float)waitTime);

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;
    }
}
