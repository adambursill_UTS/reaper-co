﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlamZoneTrigger : MonoBehaviour
{

    public GameObject slamParticles;
    public GameObject KillMinonsWave;


    private BossEvents bossEvents;
    private GameObject player;

	// Use this for initialization
	void Start ()
    {
        bossEvents = GameObject.Find("BossEvents").GetComponent<BossEvents>();
        player = GameObject.FindGameObjectWithTag("Player");
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && bossEvents.HasTalkedToPrincess)
        {
            var slamP = Instantiate(slamParticles, player.transform.position, Quaternion.identity);

            Destroy(slamP, 6f);
        }
        
    }

    public void KillAllMinions()
    {
        var slamP = Instantiate(KillMinonsWave, transform.position, Quaternion.identity);

        iTween.ScaleTo(slamP, new Vector3(150, 150, 150), 5f);

        Destroy(slamP, 6f);
    }
}
