﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Boss AI

// When activated boss with start in phase one, using only his sweep partical 
// attack, after 15-30 seconds the boss will spawn minions in cutscene and 
// reveal platform path when all minions are killed the boss will spawn more minions
// to try to encorage the player to platform. to move to the next phase the player needs
// to jump down with the touch and damage the boss. phase two boss will place mines.
// Phase three boss will do both.

public class BossController : MonoBehaviour
{
    [Header("Sweep Particle Attack")]
    public Transform sweepAttackSpawn;
    public float projectileSpeed;
    public GameObject sweepAttackProjectile;

    [Header("Mine Attack")]
    public GameObject mine;

	[Header("Health")]
	public int bossHealth;
    public GameObject shield;
	private int currentHealth;
	//private bool firstPhase = true;
	//private bool secondPhase = false;
	//private bool thirdPhase = false;
	private bool invincibilityBool = false;
	private Slider healthBar;
	//private UIManager uIManager;
	private new AudioSource audio;


    [Header("Death")]
    public GameObject deathParticals;


    private GameController gameController;
    private Animator anim;
    private enum BossState
    {
        SweepAttack,
        MineAttack,
        SpawnMinions
    }
    public enum BossPhase
    {
        Phase1,
        Phase2,
        Phase3
    }
    private BossState currentBossState;
    private BossPhase currentBossPhase;

    private GameObject player;

    private RaycastHit hit;
    private bool canSpawnMinions = false;
    private bool bossActive = false;
    private BossEvents bossEvents;

	// Use this for initialization
	void Start ()
    {
        audio = GetComponent<AudioSource> ();
		currentHealth = bossHealth;
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        currentBossPhase = BossPhase.Phase1;
        bossEvents = GameObject.Find("BossEvents").GetComponent<BossEvents>();
		gameController = GameObject.Find ("GameController").GetComponent<GameController> ();
		//uIManager = GameObject.Find ("GameController").GetComponent<UIManager> ();
	}

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawRay(sweepAttackSpawn.position, -(sweepAttackSpawn.position - player.transform.position));  

        Debug.DrawRay(sweepAttackSpawn.position, -(sweepAttackSpawn.position - player.transform.position));
    }

    public void ActivateBoss()
    {
        bossActive = true;
		healthBar = GameObject.Find ("BossHealthBar").GetComponent<Slider> ();
		healthBar.value = currentHealth;
        StartMinionSpawnTimer();
        EnteredIdle(0);
    }

    public void PlayerDiedContine()
    {
        bossActive = true;
        StartMinionSpawnTimer();   
        EnteredIdle(0.5f);
    }

    public void LowerShieldContinueAttacking()
    {
        shield.SetActive(false);
        canSpawnMinions = false;
        bossActive = true;
        EnteredIdle(2.5f);
    }

    public void ContinueMinionSpawn()
    {
        bossActive = true;
        EnteredIdle();
    }

    private void StartMinionSpawnTimer()
    {
        float spawnTimer = Random.Range(15, 30);
        StartCoroutine(minionSpawnTimer(spawnTimer));
    }

    private IEnumerator minionSpawnTimer(float time)
    {
        yield return new WaitForSeconds(time);
        canSpawnMinions = true;
    }

    // Once a state has changed execute the relevent animation that will then execute the
    // the state machine code below ie sweep and mine attacks
    public void ChangeBossState()
    {
        switch(currentBossState)
        {
            case BossState.SweepAttack:
                anim.SetTrigger("SweepAttack");
                break;
            case BossState.MineAttack:
                anim.SetTrigger("MineAttack");
                break;
            case BossState.SpawnMinions:
                // Check boss health, if below an amount change to next phase

                // Call Bossevent and start boss timeline
                bossEvents.StartBossSpawnMinionsTimeline(currentBossPhase);

                StartCoroutine(StartSpawnMininonsAnimation());
                bossActive = false;
                break;
        }
    }

    // Need to control the offset of the animation to sync up with timeline
    private IEnumerator StartSpawnMininonsAnimation()
    {
        yield return new WaitForSeconds(2f);
        anim.SetTrigger("SpawnMinions");
    }

    // Main method for controlling boss being active and changing states
    private IEnumerator IdleEnteredAsync(float initalWait)
    {
        if (bossActive)
        {
            iTween.LookTo(gameObject, new Vector3(player.transform.position.x, gameObject.transform.position.y, player.transform.position.z), 2f);
            yield return new WaitForSeconds(initalWait);

            switch (currentBossPhase)
            {
                case BossPhase.Phase1:
                    if (!canSpawnMinions)
                    {
                        currentBossState = BossState.SweepAttack;
                    }
                    else
                    {
                        yield return new WaitForSeconds(1f);
                        currentBossState = BossState.SpawnMinions;
                    }
                    break;
                case BossPhase.Phase2:
                    if (!canSpawnMinions)
                    {
                        currentBossState = BossState.MineAttack;
                    }
                    else
                    {
                        yield return new WaitForSeconds(2f);
                        currentBossState = BossState.SpawnMinions;
                    }
                    break;
                case BossPhase.Phase3:
                    if (!canSpawnMinions)
                    {
                        currentBossState = (BossState)Random.Range(0, 2);
                    }
                    else
                    {
                        yield return new WaitForSeconds(2f);
                        currentBossState = BossState.SpawnMinions;
                    }
                    break;
            }           
            ChangeBossState();
        }
    }

    // Called from animation state machine
    public void EnteredIdle(float initalWait = 1f)
    {
        StartCoroutine(IdleEnteredAsync(initalWait));
    }

    // Called from animation state machine
    public void SweepAttackStarted()
    {
        StartCoroutine(SweetAttackStartedAsync());
    }

    // Called from animation state machine
    public void MineAttackStarted()
    {
        StartCoroutine(MineAttackStartedAsync());
    }

    private IEnumerator MineAttackStartedAsync()
    {
        // Wait for animation to get to a good spot
        yield return new WaitForSeconds(.70f);

        for (int i = 0; i < 3; i++)
        {
            if(Physics.Raycast(player.transform.position, Vector3.down, out hit))
            {
                var projectileDestination = hit.point;

                var playerVelocity = player.GetComponent<CharacterController>().velocity;

                playerVelocity *= 1.2f;
                projectileDestination += new Vector3(playerVelocity.x, hit.point.y, playerVelocity.z);


                if (!float.IsNaN(projectileDestination.x))
                {
                    Instantiate(mine, projectileDestination, Quaternion.identity);
                }
                       
                //Instantiate(mine, hit.point, Quaternion.identity);
            }
            else
            {
                var projectileDestination = player.transform.position;

                var playerVelocity = player.GetComponent<CharacterController>().velocity;

                playerVelocity *= 1.2f;
                projectileDestination += playerVelocity;

                Instantiate(mine, projectileDestination, Quaternion.identity);
                //Instantiate(mine, player.transform.position, Quaternion.identity);
            }
                           
            yield return new WaitForSeconds(0.5f);
        }

    }

    private IEnumerator SweetAttackStartedAsync()
    {
        
        // Wait for animation to get to a good spot
        yield return new WaitForSeconds(.70f);

        for(int i = 0; i < 8; i++)
        {
            if(Physics.Linecast(sweepAttackSpawn.position, player.transform.position, out hit))
            {
                //Debug.Log("raycasthit");
                GameObject projectile = Instantiate(sweepAttackProjectile, sweepAttackSpawn.position, Quaternion.identity) as GameObject;
             
                var projectileDestination = new Vector3(hit.point.x, hit.point.y + 1.5f, hit.point.z);
                var distance = Vector3.Distance(sweepAttackSpawn.position, projectileDestination);

                var flightTime = distance / projectileSpeed;

                var playerVelocity = player.GetComponent<CharacterController>().velocity;

                playerVelocity *= (flightTime * 25);
                projectileDestination += playerVelocity;
                projectile.transform.LookAt(projectileDestination);

                //projectile.transform.LookAt(new Vector3(hit.point.x, hit.point.y + 1.5f, hit.point.z));
                projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * projectileSpeed);
                projectile.GetComponent<ETFXProjectileScript>().impactNormal = hit.normal;
            }     
            else
            {
                GameObject projectile = Instantiate(sweepAttackProjectile, sweepAttackSpawn.position, Quaternion.identity) as GameObject;

                var projectileDestination = player.transform.position;
                var distance = Vector3.Distance(sweepAttackSpawn.position, projectileDestination);

                var flightTime = distance / projectileSpeed;

                var playerVelocity = player.GetComponent<CharacterController>().velocity;

                playerVelocity *= (flightTime * 35);
                projectileDestination += playerVelocity;
                projectile.transform.LookAt(projectileDestination);

                //projectile.transform.LookAt(player.transform.position);
                projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * projectileSpeed);
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

	public void loseHealth(int lose) {
		if (!invincibilityBool) {
			audio.Play ();
			currentHealth -= lose;
		}
		if (currentHealth == 10)
        {
            shield.SetActive(true);
            StartCoroutine (invincibility ());
			gameController.SetBonerang ();
			currentBossPhase = BossPhase.Phase2;
            StartMinionSpawnTimer();
            EnteredIdle();
        }
		if (currentHealth == 5)
        {
            shield.SetActive(true);
            StartCoroutine (invincibility ());
			gameController.SetBonerang ();
			currentBossPhase = BossPhase.Phase3;
            StartMinionSpawnTimer();
            EnteredIdle();
        }
		if (currentHealth == 0)
        {
            StartDeathSquence();

            //gameController.GameWon();
		}

		healthBar.value = currentHealth;
	}

	float CalculateHealth() {
		return currentHealth / bossHealth;
	}

	private IEnumerator invincibility() {
		invincibilityBool = true;
		yield return new WaitForSeconds (5f);
		invincibilityBool = false;
	}

    private void StartDeathSquence()
    {
        
        //healthBar.enabled = false;
        bossActive = false;
        StopAllCoroutines();
        StartCoroutine(DeathSequence());
    }

    private IEnumerator DeathSequence()
    {
        iTween.ShakeScale(gameObject, new Vector3(0.5f, 0.5f, 0.5f), 5f);

        yield return new WaitForSeconds(1f);
        bossEvents.BossKilled();
        yield return new WaitForSeconds(4f);

        var deathVFX = Instantiate(deathParticals, transform.position, deathParticals.transform.rotation);
        deathVFX.transform.localScale = new Vector3(15f, 15f, 15f);

        bossEvents.BossDeathSequenceOver();

        Destroy(gameObject);            
    }

	public void BossPlayerDied()
    {
        Debug.Log("Player died");
        if (bossEvents.BossActiveEvents)
        {            
            StopAllCoroutines();
            bossActive = false;
            bossEvents.HasTalkedToPrincess = false;
            canSpawnMinions = false;
        }       
    }

    public void ResetBossPhase()
    {
        if(bossEvents.BossActiveEvents)
        {
            StopAllCoroutines();
            bossActive = false;
            bossEvents.HasTalkedToPrincess = false;
            canSpawnMinions = false;
            GameObject.Find("SlamZone").GetComponent<SlamZoneTrigger>().KillAllMinions();

            switch (currentBossPhase)
            {
                case BossPhase.Phase1:
                    currentHealth = 15;
                    break;
                case BossPhase.Phase2:
                    currentHealth = 10;
                    break;
                case BossPhase.Phase3:
                    currentHealth = 5;
                    break;
            }
            bossEvents.DisablePaths();

            bossEvents.playerDied = true;
            shield.SetActive(true);
            gameController.SetBonerang();
            if (healthBar != null)
            {
                healthBar.value = currentHealth;
            }
        }            
    }
}
