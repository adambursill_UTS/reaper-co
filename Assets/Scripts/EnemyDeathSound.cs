﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeathSound : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (delete ());
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private IEnumerator delete() {
		yield return new WaitForSeconds (1.0f);
		Destroy (gameObject);
	}
}
