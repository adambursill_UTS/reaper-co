﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawbridge : MonoBehaviour {

    public Animator switchAnimator;
    public Animator bridgeAnimator;
    private bool activated = false;

	private new AudioSource audio;

    private UIManager uIManager;
    private InputManager inputManager;
    private MedievalWorldEvents medievalWorldEvents;

    // Use this for initialization
    void Start () {
		audio = GetComponent<AudioSource> ();
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        uIManager = gameControllerObject.GetComponent<UIManager>();
        inputManager = gameControllerObject.GetComponent<InputManager>();

        medievalWorldEvents = GameObject.Find("MedievalWorldEvents").GetComponent<MedievalWorldEvents>();

        //switchAnimator.SetBool("SwitchOn", false);
        //bridgeAnimator.SetBool("lower", false);

    }
	
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !activated)
        {
            uIManager.ShowHintPanel();
            if (inputManager.Interact)
            {
                activated = true;
                switchAnimator.SetBool("SwitchOn", true);
				StartCoroutine (audioDelay ());
                Invoke("LowerBridge", 2);
                medievalWorldEvents.startDrawBridgeTimeline();
                uIManager.HideHintPanel();
            }
        }
    }

    private void LowerBridge()
    {
        bridgeAnimator.SetBool("lower", true);
    }
	private IEnumerator audioDelay() {
		yield return new WaitForSeconds (0.2f);
		audio.Play ();
	}
}
