﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyGroundBossControl : MonoBehaviour {

    public float stunDuration = 1f;
    public GameObject deathParticals;
	//public GameObject deathSounds;

    private Animator animator;
    private bool isDead = false;

    private NavMeshAgent agent;

    public bool CanMove { get; set; }
    private float movementSpeed;
    private GameObject player;
    private BossEvents bossEvents;

    // Use this for initialization
    void Start()
    {
        CanMove = false;
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
        bossEvents = GameObject.Find("BossEvents").GetComponent<BossEvents>();
        transform.LookAt(player.transform);
    }

    private void Update()
    {
		if (CanMove) {
			if (agent.speed == 0) {
				agent.speed = movementSpeed;
			}
			if (!GameController.isDead)
			{
				agent.SetDestination (player.transform.position);
		}
            AnimationManager.AnimateEnemyRun(animator, agent.speed);
        }
        else
        {
            if (agent.speed != 0)
            {
                movementSpeed = agent.speed;
                agent.speed = 0;
            }
        }
    }

    public void StunEnemy()
    {
        StartCoroutine(AnimationManager.AnimateEnemyStun(animator, stunDuration));
        StartCoroutine(StopEnemyMovement(stunDuration));
    }

    private IEnumerator StopEnemyMovement(float duration)
    {
        CanMove = false;
        yield return new WaitForSeconds(duration);
        if (!isDead)
        {
            CanMove = true;
        }
    }

    public void Die()
    {
        if(!isDead)
        {
            //AnimationManager.AnimateEnemyDeath(animator);
            isDead = true;
            CanMove = false;

            // Not the best way to disable it but works
            transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;

            Instantiate(deathParticals, transform.position, deathParticals.transform.rotation);
			//Instantiate (deathSounds, transform.position, Quaternion.identity);
            Destroy(gameObject);
            bossEvents.MinionKilled();
        }      
        //StartCoroutine(Die(2f));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Die();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ShockWave")
        {
            Die();
        }
    }

    private IEnumerator Die(float waitDuration)
    {
        yield return new WaitForSeconds(waitDuration);
        Destroy(gameObject);
        Destroy(transform.parent.gameObject);
    }
}
