﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CauldronScript : MonoBehaviour {

    public Animator coffeeAnimator;
    public GameObject cauldronFireParticles;
    public GameObject cauldronBubbleParticles;
    public GameObject cauldronDiveParticles;
    public MeshFilter cauldronMeshFilter;
    public Mesh newCauldronMesh;
    public float timeBetweenStartAndDive;
    public float timeBetweenDiveAndCauldronChange;

    // Use this for initialization
    void Awake () {
        coffeeAnimator.SetBool("ready", false);
        cauldronDiveParticles.SetActive(false);
        cauldronFireParticles.SetActive(false);
    }

    public IEnumerator DiveAnimationStarted() {
        yield return new WaitForSeconds(timeBetweenStartAndDive);
        cauldronDiveParticles.SetActive(true);
        StartCoroutine(DiveAnimationEnd());
    }

    private IEnumerator DiveAnimationEnd() {
        yield return new WaitForSeconds(timeBetweenDiveAndCauldronChange);
        cauldronMeshFilter.mesh = newCauldronMesh;
        cauldronFireParticles.SetActive(true);
        cauldronBubbleParticles.SetActive(false);
        Invoke("SpawnCoffee", 2);
    }

    void SpawnCoffee()
    {
        coffeeAnimator.SetBool("ready", true);
    }
}
