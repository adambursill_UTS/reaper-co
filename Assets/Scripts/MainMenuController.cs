﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public GameObject loadingScreen;
    public GameObject Menu;
    public GameObject Credits;
    public EventSystem eventSystem;
    public GameObject creditsExit;
    public Image fadeIn;

	// Use this for initialization
	void Start ()
    {
		if(PlayerPrefs.GetInt("Credits?", 0) == 1)
        {
            Menu.SetActive(false);
            Credits.SetActive(true);
            eventSystem.SetSelectedGameObject(creditsExit);
            PlayerPrefs.DeleteKey("Credits?");
            fadeIn.gameObject.SetActive(true);
            FadeInScreen(3f);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Credits.SetActive(false);
            Menu.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

    }

    public void NewGame()
    {
        loadingScreen.SetActive(true);
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void FadeInScreen(float fadeTime)
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "onupdate", "ChangeAlpha", "time", fadeTime, "easetype", iTween.EaseType.linear));
    }

    private void ChangeAlpha(float newAlpha)
    {
        fadeIn.color = new Color(0f, 0f, 0f, newAlpha);
    }

}
