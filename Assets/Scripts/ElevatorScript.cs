﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorScript : MonoBehaviour {

    private UIManager uIManager;
    public GameObject button;
    public Animator elevatorAnimator;
    private bool onElevator;

	// Use this for initialization
	void Start () {

        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        uIManager = gameControllerObject.GetComponent<UIManager>();

        elevatorAnimator.SetBool("Up", false);
        button.GetComponent<Renderer>().material.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () {

        if (onElevator == true && Input.GetButtonDown("Interact"))
        {
            elevatorAnimator.SetBool("Up", true);
            button.GetComponent<Renderer>().material.color = Color.green;
        }
	}

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            uIManager.ShowHintPanel();
            onElevator = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        uIManager.HideHintPanel();
        onElevator = false;
    }
}
