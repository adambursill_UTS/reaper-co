﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

public class DebugManager : MonoBehaviour {
    public string debugFilename = "gamedebug";
    public string debugFiletype = ".csv";
    public float debugUpdateTime = 5;
    private int boneThrowCount = 0;
    private int scytheCount = 0;
    private int rewindTimeCount = 0;
    private int blinkCount = 0;
    public bool debug;
    public static DebugManager Instance;

    private void Awake() {
        Instance = this;

        if (File.Exists(DebugToFile.filePath + debugFilename + debugFiletype)) {
            String fileData = System.IO.File.ReadAllText(DebugToFile.filePath + debugFilename + debugFiletype);
            String[] lines = fileData.Split("\n"[0]);
            String[] lineData = (lines[0].Trim()).Split(","[0]);
            int.TryParse(lineData[0], out boneThrowCount);//float.TryParse(lineData[0]);
            int.TryParse(lineData[1], out scytheCount);
            int.TryParse(lineData[2], out rewindTimeCount);
            int.TryParse(lineData[3], out blinkCount);
        }
    }

    public void IncrementBoneThrow() {
        ++boneThrowCount;
    }

    public void IncrementScytheCount() {
        ++scytheCount;
    }

    public void IncrementRewindTime() {
        ++rewindTimeCount;
    }

    public void IncrementBlink() {
        ++blinkCount;
    }

    public void Start() {
        if (debug) {
            StartCoroutine(UpdateDebugFile());
        }
    }

    private IEnumerator UpdateDebugFile() {
        yield return new WaitForSeconds(debugUpdateTime);
        string debugMessage = "" + boneThrowCount + "," + scytheCount + "," + rewindTimeCount + "," + blinkCount;
        DebugToFile.Log(debugMessage, debugFilename + debugFiletype);
        StartCoroutine(UpdateDebugFile());
    }
}
