﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerActions : PlayerActionSet
{
    public PlayerAction BoneThrow;
    public PlayerAction ScytheAttack;
    public PlayerAction Blink;
    public PlayerAction TimeReverse;
    public PlayerAction Jump;
    public PlayerAction Run;
    public PlayerAction Left;
    public PlayerAction Right;
    public PlayerAction Up;
    public PlayerAction Down;    
    public PlayerOneAxisAction MoveVertical;
    public PlayerOneAxisAction MoveHorizontal;

    public PlayerAction Interact;
    public PlayerAction Pause;

    public PlayerAction CameraUp;
    public PlayerAction CameraDown;
    public PlayerAction CameraLeft;
    public PlayerAction CameraRight;
    public PlayerOneAxisAction CameraX;
    public PlayerOneAxisAction CameraY;

    public PlayerActions()
    {
        BoneThrow = CreatePlayerAction("Bone Throw");
        ScytheAttack = CreatePlayerAction("Scythe Attack");
        Blink = CreatePlayerAction("Blink");
        TimeReverse = CreatePlayerAction("Time Reverse");

        Jump = CreatePlayerAction("Jump");
        Run = CreatePlayerAction("Run");
        Interact = CreatePlayerAction("Interact");
        Pause = CreatePlayerAction("Pause");

        Left = CreatePlayerAction("Move Left");
        Right = CreatePlayerAction("Move Right");
        Up = CreatePlayerAction("Move Up");
        Down = CreatePlayerAction("Move Down");

        MoveVertical = CreateOneAxisPlayerAction(Down, Up);
        MoveHorizontal = CreateOneAxisPlayerAction(Left, Right);

        CameraLeft = CreatePlayerAction("Camera Left");
        CameraRight = CreatePlayerAction("Camera Right");
        CameraUp = CreatePlayerAction("Camera Up");
        CameraDown = CreatePlayerAction("Camera Down");

        CameraY = CreateOneAxisPlayerAction(CameraDown, CameraUp);
        CameraX = CreateOneAxisPlayerAction(CameraLeft, CameraRight);


    }


    public static PlayerActions CreateWithDefaultBindings()
    {
        var playerActions = new PlayerActions();

        // How to set up mutually exclusive keyboard bindings with a modifier key.
        // playerActions.Back.AddDefaultBinding( Key.Shift, Key.Tab );
        // playerActions.Next.AddDefaultBinding( KeyCombo.With( Key.Tab ).AndNot( Key.Shift ) );

        playerActions.BoneThrow.AddDefaultBinding(Mouse.LeftButton);
        playerActions.BoneThrow.AddDefaultBinding(InputControlType.Action3);

        playerActions.ScytheAttack.AddDefaultBinding(Mouse.RightButton);
        playerActions.ScytheAttack.AddDefaultBinding(InputControlType.RightBumper);
		playerActions.ScytheAttack.AddDefaultBinding (InputControlType.Action2);

        playerActions.Blink.AddDefaultBinding(Key.Q);
        playerActions.Blink.AddDefaultBinding(InputControlType.LeftBumper);

        playerActions.TimeReverse.AddDefaultBinding(Key.R);
        playerActions.TimeReverse.AddDefaultBinding(InputControlType.LeftTrigger);

        playerActions.Jump.AddDefaultBinding(Key.Space);
        playerActions.Jump.AddDefaultBinding(InputControlType.Action1);

        playerActions.Run.AddDefaultBinding(Key.LeftShift);
        playerActions.Run.AddDefaultBinding(InputControlType.LeftStickButton);
		playerActions.Run.AddDefaultBinding (InputControlType.RightTrigger);

        playerActions.Interact.AddDefaultBinding(Key.E);
        playerActions.Interact.AddDefaultBinding(InputControlType.Action4);

        playerActions.Pause.AddDefaultBinding(Key.Escape);
        playerActions.Pause.AddDefaultBinding(InputControlType.Command);

        playerActions.Up.AddDefaultBinding(Key.W);
        playerActions.Down.AddDefaultBinding(Key.S);
        playerActions.Left.AddDefaultBinding(Key.A);
        playerActions.Right.AddDefaultBinding(Key.D);

        playerActions.Left.AddDefaultBinding(InputControlType.LeftStickLeft);
        playerActions.Right.AddDefaultBinding(InputControlType.LeftStickRight);
        playerActions.Up.AddDefaultBinding(InputControlType.LeftStickUp);
        playerActions.Down.AddDefaultBinding(InputControlType.LeftStickDown);

        playerActions.CameraUp.AddDefaultBinding(Mouse.PositiveY);
        playerActions.CameraDown.AddDefaultBinding(Mouse.NegativeY);
        playerActions.CameraLeft.AddDefaultBinding(Mouse.NegativeX);
        playerActions.CameraRight.AddDefaultBinding(Mouse.PositiveX);

        playerActions.CameraUp.AddDefaultBinding(InputControlType.RightStickUp);
        playerActions.CameraDown.AddDefaultBinding(InputControlType.RightStickDown);
        playerActions.CameraLeft.AddDefaultBinding(InputControlType.RightStickLeft);
        playerActions.CameraRight.AddDefaultBinding(InputControlType.RightStickRight);

        return playerActions;
    }
}
