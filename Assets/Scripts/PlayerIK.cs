﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIK : MonoBehaviour {

    private Animator anim;
    private bool rightTransitioning;
    private bool leftTransitioning;

    public GameObject rightHandIKPos;
    public GameObject leftHandIKPos;
    private float rightHandIKWeight;
    private float leftHandIKWeight;


    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        rightHandIKWeight = 0;
        rightTransitioning = false;
        leftHandIKWeight = 0;
        leftTransitioning = false;
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void OnAnimatorIK(int layerIndex) {
        anim.SetIKPosition(AvatarIKGoal.RightHand, rightHandIKPos.transform.position);
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, rightHandIKWeight);

        anim.SetIKPosition(AvatarIKGoal.LeftHand, leftHandIKPos.transform.position);
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandIKWeight);
    }

    public IEnumerator StartRightHandIK(float transitionTime) {
        if (rightTransitioning) {
            yield return new WaitForSeconds(1f);
            StartCoroutine(StartRightHandIK(transitionTime));
        } else {
            rightTransitioning = true;
            for (float t = 0f; t < 1; t += Time.deltaTime / transitionTime) {
                rightHandIKWeight = Mathf.Lerp(0f, 1f, Mathf.SmoothStep(0f, 1f, t));
                yield return null;
            }
            rightTransitioning = false;
        }
    }

    public IEnumerator StopRightHandIK(float transitionTime) {
        if (rightTransitioning) {
            yield return new WaitForSeconds(1f);
            StartCoroutine(StopRightHandIK(transitionTime));
        } else {
            rightTransitioning = true;
            for (float t = 0f; t < 1; t += Time.deltaTime / transitionTime) {
                rightHandIKWeight = Mathf.Lerp(1f, 0f, Mathf.SmoothStep(0f, 1f, t));
                yield return null;
            }
            rightTransitioning = false;
        }
    }

    public IEnumerator StartLeftHandIK(float transitionTime) {
        if (leftTransitioning) {
            yield return new WaitForSeconds(1f);
            StartCoroutine(StartLeftHandIK(transitionTime));
        } else {
            leftTransitioning = true;
            for (float t = 0f; t < 1; t += Time.deltaTime / transitionTime) {
                leftHandIKWeight = Mathf.Lerp(0f, 1f, Mathf.SmoothStep(0f, 1f, t));
                yield return null;
            }
            leftTransitioning = false;
        }
    }

    public IEnumerator StopLeftHandIK(float transitionTime) {
        if (leftTransitioning) {
            yield return new WaitForSeconds(1f);
            StartCoroutine(StopLeftHandIK(transitionTime));
        } else {
            leftTransitioning = true;
            for (float t = 0f; t < 1; t += Time.deltaTime / transitionTime) {
                leftHandIKWeight = Mathf.Lerp(1f, 0f, Mathf.SmoothStep(0f, 1f, t));
                yield return null;
            }
            leftTransitioning = false;
        }
    }
}
