﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chandelier : MonoBehaviour {

    
    public Material material;

    public GameObject blood;
    public GameObject face;

    private MedievalWorldEvents medievalWorldEvents;

    public Animator animator;
    private bool hit = false;

    private static bool one;
    private static bool two;
    private static bool three;
    private static bool four;

    private bool active;

    // Use this for initialization
    void Start () {
        animator.SetBool("fall", false);
        medievalWorldEvents = GameObject.Find("MedievalWorldEvents").GetComponent<MedievalWorldEvents>();

        one = false;
        two = false;
        three = false;
        four = false;

        active = false;
}
	
	// Update is called once per frame
	void Update () {
		    
        if (hit == true)
        {
            if(gameObject.name == "RopeHold1")
            {
                if (gameObject.transform.GetChild(2).name == "Rope1")
                {
                    GameObject.Find("Rope1").GetComponent<Renderer>().enabled = false;
                    one = true;
                    hit = false;
                }                
            }
            else if (gameObject.name == "RopeHold2")
            {
                if (gameObject.transform.GetChild(2).name == "Rope2")
                {
                    GameObject.Find("Rope2").GetComponent<Renderer>().enabled = false;
                    two = true;
                    hit = false;
                }
            }
            else if (gameObject.name == "RopeHold3")
            {
                if (gameObject.transform.GetChild(2).name == "Rope3")
                {
                    GameObject.Find("Rope3").GetComponent<Renderer>().enabled = false;
                    three = true;
                    hit = false;
                }
            }
            else if (gameObject.name == "RopeHold4")
            {
                if (gameObject.transform.GetChild(2).name == "Rope4")
                {
                    GameObject.Find("Rope4").GetComponent<Renderer>().enabled = false;
                    four = true;
                    hit = false;
                }
            }
        }

        if (one == true && two == true && three == true && four == true && !active)
        {
            active = true;
            one = false;
            two = false;
            three = false;
            four = false;
            //blood.GetComponent<Renderer>().enabled = true;
            //face.GetComponent<Renderer>().material = material;
            Invoke("fall", 1.5f);
            medievalWorldEvents.startChandelierKillTimeline();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Scythe")
        {
            hit = true;
        }
    }

    private void fall()
    {
        animator.SetBool("fall", true);
    }

}
