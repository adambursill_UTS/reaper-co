using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicWandControl : MonoBehaviour {
	public float projectileSpeed = 1000;
	//private GameObject player;
	private Vector3 endLocation;
	private Vector3 endLocationDecoy;
	private GameObject spawner;
	private bool forward;
	private bool released;
	LineRenderer lineRenderer;
	private float time = 0;
	private bool hasPlayedSound = false;
	private bool hasParticles = false;

	public AudioClip boom;

	public int lengthOfLineRenderer = 2;

	public GameObject explosion;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<ParticleSystem> ().Stop ();
		lineRenderer = GetComponent<LineRenderer> ();
		released = false;
		forward = false;
		//player = GameObject.Find ("Player");
		spawner = GameObject.Find ("BoneSpawn");
		endLocation = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z) + spawner.transform.forward * 100f;
		endLocationDecoy = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z) + spawner.transform.forward * 15f;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!released) {
			time += Time.deltaTime;
			lineRenderer.SetPosition (0, transform.position);
			lineRenderer.SetPosition (1, endLocationDecoy);
			transform.position = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z);
			endLocation = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z) + spawner.transform.forward * 100f;
			endLocationDecoy = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z) + spawner.transform.forward * 15f;
			if (time >= 1f) {
				Destroy (gameObject);
			}
		}
		if (forward) {
			time += Time.deltaTime;
			transform.position = Vector3.MoveTowards (transform.position, endLocation, Time.deltaTime * 15);
		}
		if (time >= 6.5f) {
			Destroy (gameObject);
		}

		

	}

	//public void Shoot() {
			//GameObject projectileClone = Instantiate(projectile, spawnPosition.position, Quaternion.identity) as GameObject;
			//projectile.transform.LookAt(hit.point);
			//GetComponent<Rigidbody>().AddForce(player.transform.forward * projectileSpeed);
		    //GetComponent<ETFXProjectileScript>().impactNormal = transform.forward;

	//}

	void OnCollisionEnter(Collision col) {
		Collider other = col.collider;

		if (other.tag != "Player" && other.tag != "Enemy" && other.tag != "Boss") {
			forward = false;
			StartCoroutine (explode ());

		}

		if (other.tag == "Boss") {
			forward = false;
			other.GetComponent<BossController> ().loseHealth (1);
			StartCoroutine (explode ());
		}

		if (other.tag == "Enemy") {
			forward = false;
			StartCoroutine (explode ());

		}


	}

	IEnumerator explode() {
        //gameObject.GetComponent<MeshRenderer> ().enabled = false;
        //GameObject exploder = Instantiate (explosion, transform.position, Quaternion.identity) as GameObject;
        Instantiate(explosion, transform.position, Quaternion.identity);
        yield return new WaitForSeconds (0.2f);
		Destroy (gameObject);
	}

	public void SetRelease() {
		lineRenderer.enabled = false;
		if (hasParticles == false) {
			GetComponent<ParticleSystem> ().Play ();
		}
		AudioSource audio = GetComponent<AudioSource> ();
		audio.clip = boom;
		if (hasPlayedSound == false) {
			audio.Play ();
		}
		hasParticles = true;
		hasPlayedSound = true;
		//lineRenderer.enabled = false;
		released = true;
		forward = true;
		time = 0f;
	}


}
