﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettlingChange : MonoBehaviour
{

    private Cinemachine.CinemachineFreeLook cinemachineFreeLook;

	// Use this for initialization
	void Start ()
    {
        cinemachineFreeLook = GetComponent<Cinemachine.CinemachineFreeLook>();
            
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    public void ChangeCameraYInversion(bool inverted)
    {
        cinemachineFreeLook.m_YAxis.m_InvertAxis = inverted;
    }

    public void RestCamera()
    {
        StartCoroutine(resetCamera());
    }

    public void SetBossCameraSettings()
    {
        cinemachineFreeLook.m_BindingMode = Cinemachine.CinemachineTransposer.BindingMode.LockToTargetOnAssign;
    }

    private IEnumerator resetCamera()
    {
        cinemachineFreeLook.m_BindingMode = Cinemachine.CinemachineTransposer.BindingMode.LockToTargetWithWorldUp;
        cinemachineFreeLook.m_YAxis.Value = 0.5f;
        yield return new WaitForSeconds(0.5f);
        cinemachineFreeLook.m_BindingMode = Cinemachine.CinemachineTransposer.BindingMode.SimpleFollowWithWorldUp;
    }

    public void ChangeCameraYSpeed(float value)
    {
        cinemachineFreeLook.m_YAxis.m_MaxSpeed = value;
    }

    public void ChangeCameraXSpeed(float value)
    {
        cinemachineFreeLook.m_XAxis.m_MaxSpeed = Map(value, 0, 1, 110, 310);
    }

    private float Map(float x, float inMin, float inMax, float outMin, float outMax)
    {
        return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
}
