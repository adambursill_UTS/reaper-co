﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Handles all input I Guess
public class InputManager : MonoBehaviour
{
    
    //public bool inventoryOpen = false;

    public bool Interact { set; get; }
	public static bool Respawn = false;
	private bool respawnDelay = false;
	private bool setRespawnDelay = true;

    private PlayerMovement playerMovement;
    private PlayerAbilities playerAbilities;
    private UIManager uIManager;
	private GameObject dialogueManager;
    private static PlayerActions playerActions;
    private GameController gameController;

    private InputDevice lastInputDevice;
    public bool MiniGunActive { set; get; }

    public bool LoadGame = false;
    public static bool PlayerInputsDisabled { get; set; }
    public static bool CameraInputsDisabled { get; set; }

    public enum CurrentInput
    {
        Keyboard,
        PlayStation,
        Xbox
    }

    // Use this for initialization
    void Start ()
    {
        if(LoadGame)
        {
            PlayerInputsDisabled = true;
            CameraInputsDisabled = true;
        }
        else
        {
            PlayerInputsDisabled = false;
            CameraInputsDisabled = false;
        }
        

        MiniGunActive = false;
        playerActions = PlayerActions.CreateWithDefaultBindings();
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");

		dialogueManager = GameObject.Find ("DialogueManager");
        playerMovement = playerObject.GetComponent<PlayerMovement>();
        playerAbilities = playerObject.GetComponent<PlayerAbilities>();
        gameController = GetComponent<GameController>();
        uIManager = GetComponent<UIManager>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        if(!PlayerInputsDisabled)
        {
            if ((playerActions.Interact.WasPressed && UIManager.inDialogue && !GameController.isDead) || (playerActions.Jump.WasPressed && UIManager.inDialogue && !GameController.isDead))
            {
                dialogueManager.GetComponent<DialogueManager>().DisplayNextSentence();
            }

            if (!playerAbilities.TimeRewinding && !UIManager.inDialogue && !GameController.isDead)
            {
                playerMovement.MovePlayer(playerActions.MoveHorizontal, playerActions.MoveVertical, playerActions.Jump.WasPressed, playerActions.Run.WasPressed);

                if (playerActions.Blink.WasPressed && !MiniGunActive)
                {
                    playerAbilities.Blink();
                    uIManager.BlinkUICoolDownStart();
                }

                if (playerActions.BoneThrow.WasPressed && GameController.bonerangBool)
                {
                    playerAbilities.StartBonerang();
                }
				if (playerActions.BoneThrow.WasReleased && GameController.bonerangBool && !pauseMenu.gameIsPaused)
                {
                    playerAbilities.FlyBonerang();
                }
                if (MiniGunActive && GameController.miniGunBool)
                {
                    if (playerActions.BoneThrow.WasPressed)
                    {
                        playerAbilities.StartFiringMiniGun();
                    }
                    if (playerActions.BoneThrow.WasReleased)
                    {
                        playerAbilities.StopFiringMiniGun();
                    }
                }

                if (playerActions.BoneThrow.IsPressed && GameController.magicWandBool)
                {
                    playerAbilities.ShootWand();
                }
                if (playerActions.BoneThrow.WasReleased && GameController.magicWandBool)
                {
                    playerAbilities.ReleaseWand();
                    playerAbilities.FlyBonerang();
                }

                if (playerActions.ScytheAttack.WasPressed)
                {
                    playerAbilities.StartScythe();
                }

            }

            if (UIManager.inDialogue && !playerAbilities.TimeRewinding && !GameController.isDead)
            {
                playerMovement.MovePlayer(0f, 0f, false, false);
            }

            if (GameController.isDead)
            {
                playerMovement.MovePlayer(0f, 0f, false, false);
				if (setRespawnDelay == true) {
					setRespawnDelay = false;
					StartCoroutine (respawnDelayer ());
				}
				if (playerActions.TimeReverse.WasPressed && respawnDelay == true)
                {
					respawnDelay = false;
					setRespawnDelay = true;
					dialogueManager.GetComponent<DialogueManager>().DisplayNextSentenceAfterDeath();
                    InputManager.Respawn = true;
                }
            }

			if (playerActions.TimeReverse.WasPressed && !UIManager.inDialogue && !GameController.isDead)
            {
                playerAbilities.StartTimeRewind();
            }
			if (playerActions.TimeReverse.WasReleased && !UIManager.inDialogue && !GameController.isDead)
            {
                playerAbilities.StopTimeRewind();
            }

            Interact = playerActions.Interact.WasPressed;
        }
        else
        {
            playerMovement.MovePlayer(0, 0, false, false);
        }
		
		if(playerActions.Pause.WasPressed && !UIManager.inDialogue)
        {
            gameController.PauseGame();
        }

        if(Input.GetKeyDown(KeyCode.L))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            if (CameraInputsDisabled == false)
            {
                CameraInputsDisabled = true;
            }
            else
            {
                CameraInputsDisabled = false;
            }
        }
		
        // May need this code for a later sprint
        //if (Input.GetButtonDown("Inventory"))
        //{
        //    if (inventoryOpen == false)
        //    {
        //        uIManager.inventoryPanel.SetActive(true);
        //        inventoryOpen = true;
        //    }
        //    else
        //    {
        //        uIManager.inventoryPanel.SetActive(false);
        //        inventoryOpen = false;
        //    }
        //}
    }

    public static float GetCameraInputAxis(string axis)
    {
        if (playerActions == null || CameraInputsDisabled)
            return 0;

        switch (axis)
        {
            case "Camera Y":
                return playerActions.CameraY.Value;

            case "Camera X":
                return playerActions.CameraX.Value;

        }
        return 0;
    }

    public InputManager.CurrentInput GetCurrentInputDevice()
    {
          
        InputDevice device = InControl.InputManager.ActiveDevice;

           
        if(device.Name.Contains("PlayStation"))
        {
            return CurrentInput.PlayStation;
        }
        else if(device.Name.Contains("XBox"))
        {
            return CurrentInput.Xbox;
        }

        return CurrentInput.Keyboard;

    }

	private IEnumerator respawnDelayer() {
		respawnDelay = false;
		yield return new WaitForSeconds (.1f);
		respawnDelay = true;
	}
}


