﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonerang : MonoBehaviour {

	bool forward;
	public static bool released;

	private new AudioSource audio;

	GameObject player;
	GameObject spawner;
	LineRenderer lineRenderer;

	public static bool isReturned = false;
	public int lengthOfLineRenderer = 2;

    //private GameController gameController;

    private Vector3 endLocation;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		lineRenderer = GetComponent<LineRenderer> ();
		released = false;
		isReturned = true;
		player = GameObject.Find ("Player");
		spawner = GameObject.Find ("BoneSpawn");
        //gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		//gameObject.SetActive (false);
        //StartCoroutine (Bone ());
		endLocation = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z) + spawner.transform.forward * 15f;
	}

	IEnumerator Bone() {
		forward = true;
		yield return new WaitForSeconds (1.0f);
		forward = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!released) {
			lineRenderer.SetPosition (0, transform.position);
			lineRenderer.SetPosition (1, endLocation);
			transform.position = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z);
			endLocation = new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z) + spawner.transform.forward * 15f;
		}
		if (released) {
			transform.Rotate (Vector3.right, 360f * Time.deltaTime);

			if (forward) {
				transform.position = Vector3.MoveTowards (transform.position, endLocation, Time.deltaTime * 15);
			} else if (!forward) {
				transform.position = Vector3.MoveTowards (transform.position, new Vector3 (player.transform.position.x, spawner.transform.position.y, player.transform.position.z), Time.deltaTime * 40);
			}
			if (!forward && Vector3.Distance (spawner.transform.position, transform.position) < 1.5f) {
				isReturned = false;
				Destroy (gameObject);

			}
		}
	}

	void OnCollisionEnter(Collision col) {
		//Debug.Log ("Hit!");
		Collider other = col.collider;

		if (other.tag != "Enemy") {
			forward = false;
		}

		if (other.tag == "Enemy")
        {
            other.GetComponent<EnemyBehaviour>().StunEnemy();
			forward = false;
            //Destroy (other.gameObject);
        }

        if (other.tag == "BossEnemy")
        {
            other.GetComponent<EnemyGroundBossControl>().StunEnemy();
            forward = false;
            //Destroy (other.gameObject);
        }
    }

	public void OnRelease() {
		//gameObject.SetActive (true);
		lineRenderer.enabled = false;
		audio.Play ();
		released = true;
		StartCoroutine (Bone ());
	}
}
