﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class RunTimeline : MonoBehaviour
{

    private PlayableDirector timeline;


    // Use this for initialization
    void Start()
    {
        timeline = GetComponent<PlayableDirector>();
        
        var cameraTrack = (timeline.playableAsset as TimelineAsset).GetOutputTrack(0);
        timeline.SetGenericBinding(cameraTrack, GameObject.FindGameObjectWithTag("MainCamera"));

    }

    public void PlayTimeline()
    {
		timeline.Play();
    }

	//private IEnumerator playerTimelineAfterDelay()
	//{
	//	timeline.Play();
	//}

    // Update is called once per frame
    void Update () {
		
	}
}
