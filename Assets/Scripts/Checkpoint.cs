﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {
	private GameObject gameController;
	private GameController gameScript;
    public GameObject particles;
    public GameObject spawnParticles;
    public float spawnParticlesDuration = 3f;
    public Material activeMaterial;
    public Material unactiveMaterial;
    public MeshRenderer platformMeshRenderer;
    private bool inUse;

	private int maxHealth;


    // Use this for initialization
    void Start() {
		gameController = GameObject.FindGameObjectWithTag ("GameController");
		gameScript = gameController.GetComponent<GameController> ();
        inUse = false;
        if (spawnParticles != null) {
            spawnParticles.SetActive(false);
        }
        if (platformMeshRenderer != null && unactiveMaterial != null) {
            platformMeshRenderer.material = unactiveMaterial;
        }
		maxHealth = gameController.GetComponent<GameController> ().getMaxHealth ();

    }

    public void SetInUse(bool inUse) {
        this.inUse = inUse;
    }

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "Player") {
            Checkpoint lastCheckpoint = gameScript.Checkpoint.GetComponent<Checkpoint>();
            if (!inUse) {
                if (lastCheckpoint != null) {
                    lastCheckpoint.SetInUse(false);
                    lastCheckpoint.DeactivateLights();
                }
                gameScript.UpdateCheckpoint(this.transform);
                SetInUse(true);
                ActivateLights();
            }
		}
	}

	void OnTriggerStay (Collider other) {
		if (other.tag == "Player") {
			if (gameController.GetComponent<GameController> ().getCurrentHealth () < maxHealth) {
				gameController.GetComponent<Health> ().setHealth (maxHealth);
			}
		}
	}

    public void ActivateLights() {
        if (particles != null) {
            particles.SetActive(true);
        }
        if (platformMeshRenderer != null && unactiveMaterial != null) {
            platformMeshRenderer.material = activeMaterial;
        }
    }

    public void DeactivateLights() {
        if (particles != null) {
            particles.SetActive(false);
        }
        if (platformMeshRenderer != null && unactiveMaterial != null) {
            platformMeshRenderer.material = unactiveMaterial;
        }
    }

    public void SpawnParticles() {
        if(spawnParticles != null) {
            spawnParticles.SetActive(true);
            StartCoroutine(DisableSpawnParticles());
        }
    }

    private IEnumerator DisableSpawnParticles() {
        yield return new WaitForSeconds(spawnParticlesDuration);
        if (spawnParticles != null) {
            spawnParticles.SetActive(false);
        }
    }
}
