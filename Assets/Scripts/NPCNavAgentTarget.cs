﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCNavAgentTarget : MonoBehaviour {
    private NPCGroundControl npcGroundControl;

    // Use this for initialization
    void Start() {
        npcGroundControl = transform.parent.GetComponent<NPCGroundControl>();


    }

    private void OnTriggerEnter(Collider other) {
            npcGroundControl.RandomizPatrolTarget();

    }
}
