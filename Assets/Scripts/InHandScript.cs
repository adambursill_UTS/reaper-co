﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InHandScript : MonoBehaviour {

    public GameObject rightHand;
    private bool canChangeItem = true;
    public GameObject rightHandItem;

    public void setCanChangeItem(bool canChangeItem) {
        this.canChangeItem = canChangeItem;
    }

    public bool putInHand(GameObject item) {
        if (canChangeItem) {
            clearHand();
            rightHandItem = Instantiate(item, rightHand.transform);
            return true;
        }
        return false;
    }

    public GameObject getItemInHand() {
        return rightHandItem;
    }

    public bool clearHand() {
        if (canChangeItem) {
            Destroy(rightHandItem);
            rightHandItem = null;
        }
        return false;
        }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
