﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGunControl : MonoBehaviour
{
    public float projectileSpeed = 1000;
    public float fireRatePerSecond = 1;
    public GameObject projectile;
    public Transform spawnPosition;

    private Animator anim;
    private bool gunActive;

	void Start ()
    {
        gunActive = false;
        anim = GetComponent<Animator>();
        Debug.Log("start");
	}
	
	void Update ()
    {

    }

    public void StartFiring()
    {
        gunActive = true;
        anim.SetBool("fireing", true);
        StartCoroutine(fireMiniGun());
    }

    public void StopFiring()
    {
        gunActive = false;
        anim.SetBool("fireing", false);
    }

    private IEnumerator fireMiniGun()
    {
        float perSec = 1 / fireRatePerSecond;
        
        while(gunActive)
        {
            GameObject projectileClone = Instantiate(projectile, spawnPosition.position, Quaternion.identity) as GameObject;
            //projectile.transform.LookAt(hit.point);
            projectileClone.GetComponent<Rigidbody>().AddForce(transform.forward * projectileSpeed);
            projectileClone.GetComponent<ETFXProjectileScript>().impactNormal = projectile.transform.forward;

            yield return new WaitForSeconds(perSec);
        }
    }
}
