﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// This component will change the dialog in Dialogue trigger if its method is called or if you player
// has the item matching itemRequired.
[RequireComponent(typeof(DialogueTrigger))]
public class DialogueChangePotions : MonoBehaviour
{
    public Material darkWood;
    public Material grey;
    public Material leather_material;

    public GameObject arrow;
    public GameObject leather;
    public GameObject cogs;

    public string uniqueID;
    public bool itemsAreRequired;

    // Name must match name given to the item

    public string itemRequiredOne;
    public string itemRequiredTwo;
    public string itemRequiredThree;


    public List<Dialogue> dialogues;

    private DialogueTrigger dialogueTrigger;
    private Inventory playerInventory;

    // Use this for initialization
    void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
        playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }

    // This is to be called from anything that wants to change this npc's dialog
    public void ChangeDialog()
    {
        dialogueTrigger.dialogues = this.dialogues;
        dialogueTrigger.ResetDialogueCount();
    }

    // This should be called from a dialog finished event
    public void RemoveItemFromInvetory()
    {
        if (itemsAreRequired)
        {
            playerInventory.RemoveItem(playerInventory.mItems.FirstOrDefault(x => x.Name == itemRequiredOne));
            playerInventory.RemoveItem(playerInventory.mItems.FirstOrDefault(x => x.Name == itemRequiredTwo));
            playerInventory.RemoveItem(playerInventory.mItems.FirstOrDefault(x => x.Name == itemRequiredThree));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (playerInventory.mItems.Any(x => x.Name == itemRequiredOne) && playerInventory.mItems.Any(x => x.Name == itemRequiredTwo) && playerInventory.mItems.Any(x => x.Name == itemRequiredThree))
            {
                ChangeDialog();
            }
        }
    }

    public void AddBallistaItems()
    {
        arrow.GetComponent<Renderer>().material = grey;
        leather.GetComponent<Renderer>().material = leather_material;
        cogs.GetComponent<Renderer>().material = darkWood;
    }
}
