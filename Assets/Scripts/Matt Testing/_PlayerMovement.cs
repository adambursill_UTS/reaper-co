﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class _PlayerMovement : MonoBehaviour {

    public Text helpText;
    
    public Animator castleDoor;
    public Animator leverAnimator;

    CharacterController controller;

    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public static float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;

    private int allowedJumps = 1;
    private int jumpCounter = 0;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        //Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        helpText.enabled = false;
    }

    void Update()
    {
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (controller.isGrounded)
            {
                moveDirection.y = jumpSpeed;
                jumpCounter = 0;
            }
            if (!controller.isGrounded && jumpCounter < allowedJumps)
            {
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= speed;

                moveDirection.y = jumpSpeed;
                jumpCounter++;
            }
        }

        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    


    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Lever")
        {
            helpText.enabled = true;
            if (Input.GetKeyDown(KeyCode.E))
            {
                leverAnimator.SetBool("leverDownward", true);
                castleDoor.SetBool("doors", true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        helpText.enabled = false;
    }
}
