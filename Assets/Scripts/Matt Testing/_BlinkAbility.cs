﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _BlinkAbility : MonoBehaviour {

    private GameObject player;
    private float y = 0;
    public float distance = 5.0f;

	// Use this for initialization
	void Start () {
        player = gameObject;
	}
	
	// Update is called once per frame
	void Update () {

        y = player.transform.position.y;

        if (Input.GetMouseButtonDown(0))
        {
            Blink();
        }

	}

    private void Blink()
    {
        RaycastHit hit;
        Vector3 destination = transform.position + transform.forward * distance;

        if (Physics.Linecast(transform.position, destination, out hit))
        {
            destination = transform.position + transform.forward * (hit.distance - 1f);
        }

        if (Physics.Raycast(destination, -Vector3.up, out hit))
        {
            destination = hit.point;
            destination.y = y;
            transform.position = destination;
        }
    }
}
