﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _Ladder : MonoBehaviour {       

    private GameObject player;
    private float speed = 10;

    private int ladderState = 0;
    private int reEnterState = 0;
    private bool onLadder;

    public PlayerMovement playerMovement;

    private void Start()
    {
        player = gameObject;
    }

    private void Update()
    {        
        if (onLadder == true && Input.GetKey(KeyCode.W) && ladderState == 1)
        {
            player.transform.position += Vector3.up / speed;
        }
        else if (onLadder == true && Input.GetKey(KeyCode.S) && ladderState == 1)
        {
            player.transform.position += Vector3.down / speed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LadderEntry" && ladderState == 0)
        {
            playerMovement.enabled = false;
            ladderState = 1;
            onLadder = true;
            reEnterState = 0;
        }

        if (other.tag == "LadderEntry" && reEnterState == 1 && ladderState == 1)
        {
            playerMovement.enabled = true;
            ladderState = 0;
            onLadder = false;
            reEnterState = 0;
        }

        if (other.tag == "LadderExit" && ladderState == 1 && reEnterState == 1)
        { 
            playerMovement.enabled = true;
            ladderState = 0;
            onLadder = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "LadderEntry")
        {
            reEnterState = 1;
        }

        if (other.tag == "LadderExit")
        {
            reEnterState = 0;
        }
    }

}
