﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallistaScript : MonoBehaviour {

    private static bool ready;
         
    public GameObject switchColor;

    public BoxCollider switchCollider;

    public Material red;
    public Material yellow;

    public Animator switchAnimator;
    public Animator ballistaAnimator;

    private UIManager uIManager;
    private InputManager inputManager;
    private GameController gameController;
    private MedievalWorldEvents medievalWorldEvents;

    private bool active = false;

    // Use this for initialization
    void Start ()
    {
        ready = false;
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        uIManager = gameController.GetComponent<UIManager>();
        inputManager = gameController.GetComponent<InputManager>();
        medievalWorldEvents = GameObject.Find("MedievalWorldEvents").GetComponent<MedievalWorldEvents>();

        switchCollider.enabled = false;
        switchColor.GetComponent<Renderer>().material = red;
        ballistaAnimator.SetBool("fire", false);
        switchAnimator.SetBool("SwitchOn", false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FireBallista()
    {
        ballistaAnimator.SetBool("fire", true);
    }

    public void ActivateSwitch()
    {
        ready = true;
        switchColor.GetComponent<Renderer>().material = yellow;
        switchCollider.enabled = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && ready && !active)
        {
            uIManager.ShowHintPanel();
            if (inputManager.Interact)
            {
                active = true;

                uIManager.HideHintPanel();
                switchAnimator.SetBool("SwitchOn", true);
                Invoke("FireBallista", 3);
                medievalWorldEvents.startBallistaKillTimeline();
            }
        }
    }
}
