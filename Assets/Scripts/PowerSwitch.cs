﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerSwitch : MonoBehaviour 
{       

    public Animator switchAnimator;
    public Animator YellowDoorAnimator;
    public UnityEvent onActive;

	private new AudioSource audio;

	public AudioClip garageDoor;

    private bool leverOn = false;

    private UIManager uIManager;
    private InputManager inputManager;
	public bool useThreeSecondTimer = false;


    // Use this for initialization
    void Start () 
	{
		audio = GetComponent<AudioSource> ();
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        uIManager = gameControllerObject.GetComponent<UIManager>();
        inputManager = gameControllerObject.GetComponent<InputManager>();

        switchAnimator.SetBool("SwitchOn", false);

        if (gameObject.name.ToString() == "Switch")
        {
            YellowDoorAnimator.SetBool("Yellowdoor", false);
        }
    }
	
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !leverOn)
        {
            uIManager.ShowHintPanel();
            
            if (inputManager.Interact)
            {
                leverOn = true;
                switchAnimator.SetBool("SwitchOn", true);
				StartCoroutine (audioDelay ());
				if(useThreeSecondTimer)
				{
					Invoke("GarageDoor", 3f);
				}
				else
				{
					Invoke("GarageDoor", 0.8f);
				}
                onActive.Invoke();
                uIManager.HideHintPanel();
            }
        }       
    }

    private void OnTriggerExit(Collider other)
    {
        uIManager.HideHintPanel();
    }

    private void GarageDoor()
    {
        if (gameObject.name.ToString() == "Switch")
        {
            YellowDoorAnimator.SetBool("Yellowdoor", true);
        }        
    }

	private IEnumerator audioDelay() 
	{
		yield return new WaitForSeconds (0.2f);
		audio.Play ();
		if (useThreeSecondTimer)
		{
			yield return new WaitForSeconds(2f);
		}
		else
		{
			yield return new WaitForSeconds(0.8f);
		}
		audio.clip = garageDoor;
		audio.Play ();
	}
}
