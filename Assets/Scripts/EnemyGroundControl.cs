﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyGroundControl : MonoBehaviour
{

    public int PatrolWidth;
    public int PatrolLength;
    private float movementSpeed;
    public bool canMove = true;

	//private new AudioSource audio;

    private NavAgentTarget target;
    private NavMeshAgent agent;

    private NavMeshPath navMeshPath;
    private GameObject player;

    private Animator animator;

	private bool audioOn = false;

    public bool ChasePlayer { get; set; }

    void Start()
    {
		//audio = GetComponent<AudioSource> ();
        player = GameObject.FindGameObjectWithTag("Player");
        target = GetComponentInChildren<NavAgentTarget>();
        agent = GetComponentInChildren<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
        RandomizPatrolTarget();
        animator = GetComponentInChildren<Animator>();
    }

    void Update() {
        if (canMove)
        {
            if(agent.speed == 0)
            {
                agent.speed = movementSpeed;
            }
            if (ChasePlayer)
            {
                agent.SetDestination(player.transform.position);
				if (audioOn == false) {
					startAudio ();
				}
            }
            else
            {
                agent.SetDestination(target.transform.position);
				if (audioOn == true) {
					stopAudio ();
				}
            }
            AnimationManager.AnimateEnemyRun(animator, agent.speed);
        }
        else
        {
            if (agent.speed != 0) {
                movementSpeed = agent.speed;
                agent.speed = 0;
            }
        }
    }

    public void RandomizPatrolTarget()
    {

        target.transform.position = new Vector3(Random.Range(transform.position.x - PatrolWidth / 2,
            transform.position.x + PatrolWidth / 2), transform.position.y, Random.Range(transform.position.z - PatrolLength / 2, transform.position.z + PatrolLength / 2));       

        // Can the agent get to the location if not randomize another
        //CAUSES CRASH WHEN GAME TRIES TO RANDOMISE TARGET WHEN ENEMY IS DEAD.
        //if (agent.CalculatePath(target.transform.position, navMeshPath) == false)
        //{
        //    RandomizPatrolTarget();
        //}
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(PatrolWidth, 1, PatrolLength));
    }

	void startAudio() {
		audioOn = true;
		agent.GetComponent<AudioSource> ().Play ();
	}

	void stopAudio() {
		audioOn = false;
		agent.GetComponent<AudioSource> ().Stop ();
	}
}
