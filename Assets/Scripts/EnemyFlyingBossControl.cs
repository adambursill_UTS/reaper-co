﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyingBossControl : MonoBehaviour
{
    public float movementSpeed;

    public bool CanMove { get; set; }

    private GameObject player;
    private BossEvents bossEvents;

    private bool dead = false;

    // Use this for initialization
    void Start ()
    {
        CanMove = false;
        player = GameObject.FindGameObjectWithTag("Player");
        StartCoroutine(Move());
        bossEvents = GameObject.Find("BossEvents").GetComponent<BossEvents>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //iTween.MoveUpdate(gameObject, iTween.Hash("position", player.transform.position, "time", movementSpeed,
        //    "looktarget", player.transform.position, "looktime", .6));
	}

    public void HasDied()
    {
        if(!dead)
        {
            dead = true;
            bossEvents.MinionKilled();
            Destroy(gameObject);
        }

        
    }

    private IEnumerator Move()
    {
        while(true)
        {
			if(CanMove && !GameController.isDead)
            {
                iTween.MoveTo(gameObject, iTween.Hash("position", player.transform.position, "speed", movementSpeed,
               "looktarget", player.transform.position, "looktime", .6, "easetype", iTween.EaseType.linear));
            }           
            yield return new WaitForSeconds(0.5f);
        }
    }
}
