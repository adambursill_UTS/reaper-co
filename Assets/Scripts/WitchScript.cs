﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchScript : MonoBehaviour {

    public CauldronScript cauldronScript;
    public Animator animator;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void JumpIntoCauldron() {
        foreach (Collider col in GetComponentsInChildren<Collider>()) {
            col.enabled = false;
        }
        GetComponent<DialogueTrigger>().enabled = false;
        animator.SetTrigger("Dive");
        StartCoroutine(cauldronScript.DiveAnimationStarted());
        GameObject.FindGameObjectWithTag("GameController").GetComponent<UIManager>().HideHintPanel();
        this.enabled = false;
    }
}
