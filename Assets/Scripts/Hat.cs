﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hat : MonoBehaviour
{
    private Animator hatAnimator;

	// Use this for initialization
	void Start ()
    {
        hatAnimator = GetComponent<Animator>();

    }
	
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bone")
        {
            hatAnimator.SetBool("hit", true);
        }
    }

}
