﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class MedievalWorldEvents : MonoBehaviour
{
    public TheReapingTransformation reapingTransformation;

    [Header("DrawBridgeCutScene")]
    public PlayableDirector drawBridgeTimeline;

    [Header("BallistaKillCutScene")]
    public PlayableDirector ballistaKillTimeline;

    [Header("ChandelierKillCutScene")]
    public PlayableDirector chandelierKillTimeline;

    [Header("Collectathon")]
    public GameObject milkInventoryItem;
    public GameObject ThroneRoomGuard;
    public GameObject CostumVendor;
	public GameObject TargetNPC;
    private DialogueChangeListener[] costumVendorDialogChanges;
    private DialogueChangeListener[] throneRoomGuardDialogChanges;
	private DialogueChangeListener[] targetNPCDialogChanges;
    private bool hasDress = false;
    private bool hasHat = false;

    private PlayerCostumeChange playerCostumeChange;
    private int milkCollected;
    private Inventory playerInventory;

	private bool targetChange = false;

    private bool hasRun = false;

	// Use this for initialization
	void Start ()
    {
        var cameraTrack = (drawBridgeTimeline.playableAsset as TimelineAsset).GetOutputTrack(0);
        drawBridgeTimeline.SetGenericBinding(cameraTrack, GameObject.FindGameObjectWithTag("MainCamera"));

        cameraTrack = (ballistaKillTimeline.playableAsset as TimelineAsset).GetOutputTrack(0);
        ballistaKillTimeline.SetGenericBinding(cameraTrack, GameObject.FindGameObjectWithTag("MainCamera"));

        cameraTrack = (chandelierKillTimeline.playableAsset as TimelineAsset).GetOutputTrack(0);
        chandelierKillTimeline.SetGenericBinding(cameraTrack, GameObject.FindGameObjectWithTag("MainCamera"));

        throneRoomGuardDialogChanges = ThroneRoomGuard.GetComponents<DialogueChangeListener>();
        costumVendorDialogChanges = CostumVendor.GetComponents<DialogueChangeListener>();
		targetNPCDialogChanges = TargetNPC.GetComponents<DialogueChangeListener> (); 

        var playerObj = GameObject.FindGameObjectWithTag("Player");
        playerCostumeChange = playerObj.GetComponent<PlayerCostumeChange>();
        playerInventory = playerObj.GetComponent<Inventory>();

    }

	void Update() {
		if (TargetPuzzleScript.targetsHit >= 3 && targetChange == false) {
			foreach (var d in targetNPCDialogChanges) {
				if (d.uniqueID == "hitTargets") {
					d.ChangeDialog ();
					targetChange = true;
				}
			}
		}
	}


    public void ReceivedDress()
    {
        playerCostumeChange.CostumeChangeToPrincess();
        hasDress = true;
        if(hasDress && hasHat)
        {
            foreach(var d in throneRoomGuardDialogChanges)
            {
                if(d.uniqueID == "final")
                {
                    d.ChangeDialog();
                }
            }
        }
        else
        {
            foreach (var d in throneRoomGuardDialogChanges)
            {
                if (d.uniqueID == "hasDress")
                {
                    d.ChangeDialog();
                }
            }
        }
    }

    public void ReceivedHat()
    {
        playerCostumeChange.WearPrincessHat(true);
        hasHat = true;
        if (hasDress && hasHat)
        {
            foreach (var d in throneRoomGuardDialogChanges)
            {
                if (d.uniqueID == "final")
                {
                    d.ChangeDialog();
                }
            }
        }
        else
        {
            foreach (var d in throneRoomGuardDialogChanges)
            {
                if (d.uniqueID == "hasHat")
                {
                    d.ChangeDialog();
                }
            }
        }
    }

    public void MilkCollected()
    {
        milkCollected++;

        foreach(var d in costumVendorDialogChanges)
        {
            if(d.uniqueID == milkCollected.ToString())
            {
                d.ChangeDialog();
            }
        }
    }

    public void RemoveMilkFromInventory()
    {
        for(int i = 0; i < milkCollected; i++)
        {
            playerInventory.RemoveItem(playerInventory.mItems.FirstOrDefault(x => x.Name == "Milk"));
        }
        
    }

    public void BoneThrowingGalleryCompleted()
    {
        var g = Instantiate(milkInventoryItem, transform);
        IInventoryItem item = g.GetComponent<IInventoryItem>();

        playerInventory.AddItem(item);
        MilkCollected();
    }

    public void FlowerColectionCompleted()
    {
        var g = Instantiate(milkInventoryItem, transform);
        IInventoryItem item = g.GetComponent<IInventoryItem>();

        playerInventory.AddItem(item);
        MilkCollected();
    }

    //public void CoffeeDeath()
    //{
    //    Invoke("EndGame", 3);
    //}

    //private void EndGame()
    //{
    //    gameController.LoadBossLevel();
    //}

    public void startDrawBridgeTimeline()
    {
        drawBridgeTimeline.Play();

        //StartCoroutine(enableControlsAfterTimeline(drawBridgeTimeline.duration));
    }

    public void startBallistaKillTimeline()
    {
        ballistaKillTimeline.Play();

        StartCoroutine(startTransformAfterTimeline((float)ballistaKillTimeline.duration));
    }

    public void startChandelierKillTimeline()
    {
        if(!hasRun)
        {
            hasRun = true;
            chandelierKillTimeline.Play();
            StartCoroutine(startTransformAfterTimeline((float)chandelierKillTimeline.duration));
        }
    }

    private IEnumerator startTransformAfterTimeline(float wait)
    {
        yield return new WaitForSeconds(wait - 4.5f);
        reapingTransformation.StartReapingSequence();
    }

    private IEnumerator enableControlsAfterTimeline(double waitTime)
    {
        InputManager.PlayerInputsDisabled = true;
        InputManager.CameraInputsDisabled = true;

        yield return new WaitForSeconds((float)waitTime);

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;
    }
}
