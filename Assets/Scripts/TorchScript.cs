﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchScript : MonoBehaviour {

    private bool hasTorch;
    public MeshRenderer torch;
    public ParticleSystem[] torchParticles;

	// Use this for initialization
	void Start () {
        hasTorch = false;
        torch.enabled = false;
        foreach(ParticleSystem particle in torchParticles){
            particle.Stop();
        }
    }

    public void GiveTorch() {
        hasTorch = true;
    }

    public void RemoveTorch() {
        hasTorch = false;
    }

    // Update is called once per frame
    void Update () {
        if (hasTorch) {
            if (!torch.enabled) {
                torch.enabled = true;
                foreach (ParticleSystem particle in torchParticles) {
                    particle.Play();
                }
            }
        } else {
            if (torch.enabled) {
                torch.enabled = false;
                foreach (ParticleSystem particle in torchParticles) {
                    particle.Stop();
                }
            }
        }
	}
}
