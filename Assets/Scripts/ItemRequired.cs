﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemRequired : MonoBehaviour
{
    public string ItemName;
    public GameObject item;

    private Inventory playerInventory;
    private UIManager uIManager;
    private InputManager inputManager;

	// Use this for initialization
	void Start ()
    {
        playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();

        var gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        uIManager = gameControllerObject.GetComponent<UIManager>();
        inputManager = gameControllerObject.GetComponent<InputManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerStay(Collider other)
    {
        if(playerInventory.mItems.Any(x => x.Name == ItemName))
        {
            uIManager.ShowHintPanel();

            if(inputManager.Interact)
            {
                GameObject gameObject = Instantiate(item, transform.position, Quaternion.identity);

                Destroy(gameObject.GetComponent<PickupOutline>());
                gameObject.tag = "Untagged";
                playerInventory.RemoveItem(playerInventory.mItems.FirstOrDefault(x => x.Name == ItemName));
                uIManager.HideHintPanel();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        uIManager.HideHintPanel();
    }

}
