﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMineDamage : MonoBehaviour {
	private GameController gameController;

	void Start () {
		gameController = GameObject.Find ("GameController").GetComponent<GameController> ();
	}
	// Use this for initialization
	void OnTriggerEnter(Collider col) {
		if (col.tag == "Player") {
			gameController.loseHealth (1);
		}
	}
}
