﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PickUpAble : MonoBehaviour
{
    // Called from inventory script
    public UnityEvent OnPickUp;
}
