﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayerTrigger : MonoBehaviour
{

    private EnemyGroundControl enemyGroundControl;

	// Use this for initialization
	void Start ()
    {
        enemyGroundControl = transform.parent.parent.GetComponent<EnemyGroundControl>();

    }
	
	// Update is called once per frame
	void Update () {
		if (GameController.isDead == true) {

			enemyGroundControl.ChasePlayer = false;
		}
	}

    private void OnTriggerEnter(Collider other)
    {
		if (!GameController.isDead) {
			if (other.tag == "Player") {
				enemyGroundControl.ChasePlayer = true;
			}
		}
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            enemyGroundControl.ChasePlayer = false;
        }
    }
}
