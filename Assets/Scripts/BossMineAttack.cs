﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMineAttack : MonoBehaviour
{
    public GameObject mineRunePartical;
    public GameObject mineExplosionPartical;
    public float fuseTime = 1;

    private new CapsuleCollider collider;
    // Use this for initialization
    void Start ()
    {
        StartCoroutine(mineSequence());
        collider = GetComponent<CapsuleCollider>();
	}
	
    private IEnumerator mineSequence()
    {
        var partical = Instantiate(mineRunePartical, new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z), mineRunePartical.transform.rotation);
        yield return new WaitForSeconds(fuseTime);
        Destroy(partical);
        collider.enabled = true;
        Instantiate(mineExplosionPartical, new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z), mineExplosionPartical.transform.rotation);
        Destroy(gameObject, 1f);
    }
}
