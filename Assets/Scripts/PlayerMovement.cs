﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    CharacterController controller;
    //Animator characterAnimator;

    public float normalSpeed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float runSpeed = 8.0f;
    public static float gravity = 20.0F;
    public float turnSpeed;

	public AudioClip jumping;

    public Transform playerModel;
    public Transform followCamera;

    private Vector3 moveDirection = Vector3.zero;

    private int allowedJumps = 2;
    private int jumpCounter = 0;
    private bool jump = false;
    private bool lastFrameGrounded = true;
    private float currentSpeed;


    private void Start()
    {
        controller = GetComponent<CharacterController>();
        //characterAnimator = GetComponentInChildren<Animator>();
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;
        StartCoroutine(CheckIfGrounded());
    }

    // Called from input manager in Update
    // Apparently Player Controller use's update to move FYI.
    public void MovePlayer(float moveHorizontal, float moveVertical, bool jumpButton, bool runActive)
    {
        if(runActive)
        {
            if (currentSpeed != runSpeed) {
                currentSpeed = runSpeed;
                AnimationManager.AnimatePlayerSprinting(true);
            } else {
                currentSpeed = normalSpeed;
                AnimationManager.AnimatePlayerSprinting(false);
            }
        }
        if (moveHorizontal == 0 && moveVertical == 0)
        {
            currentSpeed = normalSpeed;
            AnimationManager.AnimatePlayerSprinting(false);
        }

        if (controller.isGrounded)
        {
            moveDirection = new Vector3(moveHorizontal, 0, moveVertical);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = Vector3.ClampMagnitude(moveDirection, 1);
            moveDirection *= currentSpeed;
            jumpCounter = 0;
        }
        else if (!controller.isGrounded)
        {
            moveDirection.x = moveHorizontal * currentSpeed;
            moveDirection.z = moveVertical * currentSpeed;
            moveDirection = transform.TransformDirection(moveDirection);
        }

        

        jump = false;
        if (jumpButton)
        {
            if(controller.isGrounded)
            {   
				GetComponent<AudioSource> ().clip = jumping;
				GetComponent<AudioSource> ().Play ();
				
                moveDirection.y = jumpSpeed;
                //jumpCounter = 0;
                jumpCounter++;
                jump = true;
            } 
            if(!controller.isGrounded && jumpCounter < allowedJumps)
            {
				GetComponent<AudioSource> ().clip = jumping;
				GetComponent<AudioSource> ().Play ();
                moveDirection.y = jumpSpeed;
                jumpCounter++;
                jump = true;
            }
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
        AnimationManager.AnimatePlayerMovement(moveHorizontal, moveVertical);
        AnimationManager.AnimatePlayerJump(jump);


        // Move the player in different directions based on camera look direction
        if (moveHorizontal != 0 || moveVertical != 0)
        {
            transform.rotation = Quaternion.Euler(0f, followCamera.rotation.eulerAngles.y, 0f);
            Vector3 lookRotation = new Vector3(moveDirection.x, 0f, moveDirection.z);

            if (lookRotation != Vector3.zero)
            {
                playerModel.rotation = Quaternion.Slerp(
                playerModel.rotation,
                Quaternion.LookRotation(lookRotation),
                Time.deltaTime * turnSpeed);
            }
        }
    }

    IEnumerator CheckIfGrounded() {
        yield return new WaitForEndOfFrame();
        AnimationManager.AnimatePlayerGrounded(lastFrameGrounded || controller.isGrounded);
        lastFrameGrounded = controller.isGrounded;
        StartCoroutine(CheckIfGrounded());
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "GreenPlatform")
        {
            gameObject.transform.parent = other.gameObject.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        gameObject.transform.parent = null;
    }

}
