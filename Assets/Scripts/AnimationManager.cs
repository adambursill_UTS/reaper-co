﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour {

    public static Animator playerAnimator;

    private void Awake() {
        playerAnimator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
    }

    public static void AnimateEnemyRun(Animator enemy, float moveSpeed) {
        enemy.SetBool("running", (moveSpeed != 0));
    }

    public static void AnimateEnemyDeath(Animator enemy) {
        enemy.SetTrigger("die");
    }

    public static IEnumerator AnimateEnemyStun(Animator enemy, float stunDuration) {
        enemy.SetBool("stunned", true);
        yield return new WaitForSeconds(stunDuration);
        enemy.SetBool("stunned", false);
    }

    public static void AnimatePlayerMovement(float moveHorizontal, float moveVertical) {

        playerAnimator.SetFloat("vertical movement", moveVertical);
        playerAnimator.SetFloat("horizontal movement", moveHorizontal);
        //if (moveHorizontal != 0 || moveVertical != 0) {
        //    animator.SetFloat("vertical movement", moveHorizontal);
        //    //animator.SetBool("running", true);
        //} else {
        //    animator.SetBool("running", false);
        //}
        if (moveHorizontal == 0 && moveVertical == 0) {
            playerAnimator.SetBool("running", false);
        } else {
            playerAnimator.SetBool("running", true);
        }
    }

    public static void AnimatePlayerJump(bool jump) {
        playerAnimator.SetBool("jumping", jump);
    }

    public static void AnimateBoneAttack(bool boneAttack) {
        playerAnimator.SetBool("boneAttack", boneAttack);
    }

    public static void AnimateScytheAttack(bool scytheAttack) {
        playerAnimator.SetBool("scytheAttack", scytheAttack);
    }

    public static void AnimatePlayerGrounded(bool grounded) {
        playerAnimator.SetBool("grounded", grounded);
    }

    public static void AnimatePlayerSprinting(bool sprinting) {
        if (sprinting) {
            playerAnimator.SetFloat("sprintingSpeed", 2);
        } else {
            playerAnimator.SetFloat("sprintingSpeed", 1);
        }
    }

    public static void AnimatePlayerDeath(bool death) {
        playerAnimator.SetBool("death", death);
    }

    public static void AnimateMinigunHoldingWithoutIK(bool holding) {
        playerAnimator.SetBool("minigun", holding);
    }
}
