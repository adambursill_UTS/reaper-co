﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IronGate : MonoBehaviour {

    public Animator switchAnimator;
    public Animator ironGateAnimator;
    public UnityEvent onSwitchActivate;

	// Use this for initialization
	void Start () {
        //ironGateAnimator.SetBool("raise", false);
        //switchAnimator.SetBool("SwitchOn", false);
    }
	
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bone")
        {
            switchAnimator.SetBool("SwitchOn", true);
            ironGateAnimator.SetBool("raise", true);
            onSwitchActivate.Invoke();
        }
    }

}
