﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrincessScript : MonoBehaviour {
	GameController gameController;

	void Start() {
		gameController = GameObject.Find ("GameController").GetComponent<GameController> ();
	}
	public void Wanderful() {
		gameController.SetMagicWand ();
	}
}
