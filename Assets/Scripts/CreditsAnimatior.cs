﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsAnimatior : MonoBehaviour
{
    public float StartingPostionY;
    public float EndPostionY;
    public float scrollSpeed;

    private RectTransform rectTransform;
    // Use this for initialization
    void Start ()
    {
        rectTransform = GetComponent<RectTransform>();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {  
        iTween.ValueTo(gameObject, iTween.Hash("from", StartingPostionY, "to", EndPostionY, "onupdate", "MoveText",  "speed", scrollSpeed, "easetype", iTween.EaseType.linear));
    }

    void MoveText(float newPosition)
    {
        rectTransform.anchoredPosition = new Vector3(0f, newPosition, 0f);
    }

    private void OnDisable()
    {
        iTween.Stop();
        rectTransform.anchoredPosition = new Vector3(0f, StartingPostionY, 0f);
    }
}
