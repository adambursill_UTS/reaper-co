﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// Handles anything to do with UI, eg showing text, changing texts, etc
public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject inventoryPanel;
    private Animator inventoryPanelAnim;
    
    public pauseMenu pauseMenu;

	public GameObject bossHealthBar;

    private float blinkCoolDown;
    public Image blinkCoolDownIcon;
    private float rewindCoolDown;
    public Image rewindTimeIcon;

	public TextMeshProUGUI LivesRemaining;
    public GameObject GameWonPanel;

	public GameObject dialoguePanel;

	public static bool inDialogue = false;

    public GameObject ReapingText;

    public GameObject FadeOut;

    [Header("Hint Panel")]
    [SerializeField]
    private GameObject hintPanel;
    public TextMeshProUGUI hintPanelText;
    public Image controllerButtonUI;
    public Sprite psImage;
    public Sprite xboxImage;

    [Header("UI Button Promps")]
    public Image BlinkB;
    public Image ReverseB;
    public Sprite keyboardQ;
    public Sprite keyboardR;
    public Sprite psL1;
    public Sprite psL2;
    public Sprite xboxLB;
    public Sprite xboxLT;

    private InputManager inputManager;
    private PlayerAbilities playerAbilities;



    private void Awake()
    {
        playerAbilities = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAbilities>();

        blinkCoolDown = playerAbilities.blinkCooldown;

        rewindCoolDown = playerAbilities.timeRewindCoolDown;
    }

    // Use this for initialization
    void Start ()
    {
        inDialogue = false;
        inventoryPanelAnim = inventoryPanel.GetComponent<Animator>();
        inputManager = GetComponent<InputManager>();

        hintPanel.SetActive(false);
        rewindTimeIcon.color = Color.cyan;
    }

    public void ShowHintPanel()
    {
        switch (inputManager.GetCurrentInputDevice())
        {
            case InputManager.CurrentInput.Keyboard:
                controllerButtonUI.enabled = false;
                hintPanelText.text = "- Press 'E' To Interact -";
                BlinkB.sprite = keyboardQ;
                ReverseB.sprite = keyboardR;
                BlinkB.enabled = true;
                ReverseB.enabled = true;
                break;
            case InputManager.CurrentInput.PlayStation:
                controllerButtonUI.enabled = true;
                controllerButtonUI.sprite = psImage;
                hintPanelText.text = "- Press         To Interact -";
                BlinkB.sprite = psL1;
                ReverseB.sprite = psL2;
                BlinkB.enabled = true;
                ReverseB.enabled = true;
                break;
            case InputManager.CurrentInput.Xbox:
                hintPanelText.text = "- Press         To Interact -";
                controllerButtonUI.sprite = xboxImage;
                BlinkB.sprite = xboxLB;
                ReverseB.sprite = xboxLT;
                BlinkB.enabled = true;
                ReverseB.enabled = true;
                break;
        }

        hintPanel.SetActive(true);
    }

    public void HideHintPanel()
    {
        hintPanel.SetActive(false);
    }

	public void ShowBossHealth()
	{
		bossHealthBar.SetActive (true);
	}

    public void ShowInventoryPanel()
    {
        inventoryPanelAnim.SetBool("Active", true);
    }

    public void HideInventoryPanel()
    {
        inventoryPanelAnim.SetBool("Active", false);
    }

    public void UpdateLivesRemainingUI(int lives) {
		LivesRemaining.text = lives.ToString ("00");
	}

    public void BlinkUICoolDownStart()
    {
        StartCoroutine(IncrementBlinkUI());
    }

    public void RewindTimeUICoolDownStart()
    {
        StartCoroutine(IncrementRewindUIColor());
    }

    public void PauseMenu()
    {
        pauseMenu.PauseOrResumeGame();
    }



    public void UpdateTimeRewindIcon(float timeRewindLeft)
    {
        //Debug.Log(timeRewindLeft * 1 / playerAbilities.timeRewindAmount);
        rewindTimeIcon.fillAmount = timeRewindLeft * 1 / playerAbilities.timeRewindAmount;

        //blinkCoolDownIcon.fillAmount = timeRewindLeft * 1 / playerAbilities.timeRewindAmount;


        //Debug.Log("timeRewindLeft: " + timeRewindLeft + " rewindTimeIcon.fillAmount: " + rewindTimeIcon.fillAmount);
    }

    private IEnumerator IncrementBlinkUI()
    {
        blinkCoolDownIcon.fillAmount = 0;
        blinkCoolDownIcon.color = Color.red;


        float coolDownTime = 0;

        while (blinkCoolDownIcon.fillAmount < 1)
        {
            coolDownTime += Time.deltaTime;
            blinkCoolDownIcon.fillAmount = coolDownTime / blinkCoolDown;
            yield return null;
        }

        blinkCoolDownIcon.color = Color.green;
    }

    private IEnumerator IncrementRewindUIColor()
    {   
        //rewindTimeIcon.color = Color.red;
        float coolDownTime = 0;

        while (coolDownTime < rewindCoolDown)
        {
            coolDownTime += Time.deltaTime;
            rewindTimeIcon.color = Color.Lerp(Color.red, Color.cyan, coolDownTime / rewindCoolDown);
            yield return null;
        }

        //rewindTimeIcon.color = Color.green;
    }

    public void FadeOutScreen(float fadeTime)
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 1, "onupdate", "ChangeAlpha", "time", fadeTime, "easetype", iTween.EaseType.linear));
    }

    private void ChangeAlpha(float newAlpha)
    {
        FadeOut.GetComponent<Image>().color = new Color(0f, 0f, 0f, newAlpha);
    }
}
