﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCGroundControl : MonoBehaviour {

    public int PatrolWidth;
    public int PatrolLength;
    private float movementSpeed;
    public bool canMove = true;

    private NPCNavAgentTarget target;
    private NavMeshAgent agent;

    private NavMeshPath navMeshPath;
    private Animator animator;

    void Start() {
        target = GetComponentInChildren<NPCNavAgentTarget>();
        agent = GetComponentInChildren<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
        RandomizPatrolTarget();
        animator = GetComponentInChildren<Animator>();
    }

    void Update() {
        if (animator.GetBool("Talking")) {
            canMove = false;
        } else {
            canMove = true;
        }
        if (canMove) {
            if (agent.speed == 0) {
                agent.speed = movementSpeed;
            }
            agent.SetDestination(target.transform.position);
        } else {
            if (agent.speed != 0) {
                movementSpeed = agent.speed;
                agent.speed = 0;
            }
        }
    }

    public void RandomizPatrolTarget() {
        target.transform.position = new Vector3(Random.Range(transform.position.x - PatrolWidth / 2,
            transform.position.x + PatrolWidth / 2), transform.position.y, Random.Range(transform.position.z - PatrolLength / 2, transform.position.z + PatrolLength / 2));

        // Can the agent get to the location if not randomize another
        if (agent.CalculatePath(target.transform.position, navMeshPath) == false)
        {
            RandomizPatrolTarget();
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(PatrolWidth, 1, PatrolLength));
    }
}

