﻿using Cinemachine.Timeline;
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class BossEvents : MonoBehaviour
{
    [Header("Boss")]
    public PlayableDirector bossSpawnMinonsTimeline;
    public int cinemachineTackNumber;
    public CinemachineVirtualCamera CMPath1;
    public CinemachineVirtualCamera CMPath2;
    public CinemachineVirtualCamera CMPath3;

    public GameObject Path1;
    public GameObject Path2;
    public GameObject Path3;

    public int BossSpawnAmount;

    public GameObject BossIntro;
    public GameObject BossOutro;
    public GameObject DeathOutro;
    public GameObject DeathIntro;
    public GameObject DeathPart2;

    public GameObject EndGameParticals;

    private UIManager uIManager;

    [Header("MiniGunTimeline")]
    public PlayableDirector miniGunSequence;
    public int MiniGunSpawnAmount;
    public GameObject deathDialogAfterMiniOut;
    public GameObject pathBlock;

    [Header("DutchRollTimeline")]
    public PlayableDirector dutchRoll;

    private CinemachineShot platformPathShot;
    private TrackAsset pathTrack;
    private int minionsKilled;

    private BossController bossController;

    private PlayerAbilities playerAbilities;
    private GameController gameController;

    public bool BossActiveEvents { get; set; }
    public bool HasTalkedToPrincess { get; set; }

    public bool playerDied { get; set; }

    // Use this for initialization
    void Start ()
    {
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController");

        uIManager = gameControllerObj.GetComponent<UIManager> ();
        gameController = gameControllerObj.GetComponent<GameController>();

        BossActiveEvents = false;
        playerDied = false;

        bossController = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        playerAbilities = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAbilities>();

        var cameraTrack = (bossSpawnMinonsTimeline.playableAsset as TimelineAsset).GetOutputTrack(cinemachineTackNumber);
        bossSpawnMinonsTimeline.SetGenericBinding(cameraTrack, GameObject.FindGameObjectWithTag("MainCamera"));

        cameraTrack = (miniGunSequence.playableAsset as TimelineAsset).GetOutputTrack(cinemachineTackNumber);
        miniGunSequence.SetGenericBinding(cameraTrack, GameObject.FindGameObjectWithTag("MainCamera"));

        pathTrack = (bossSpawnMinonsTimeline.playableAsset as TimelineAsset).GetOutputTrack(3);
        bossSpawnMinonsTimeline.SetGenericBinding(pathTrack, Path1);

        var cameraFollor = GameObject.Find("CM FreeLook follow");

        var camAnimatTrack = (dutchRoll.playableAsset as TimelineAsset).GetOutputTrack(0);
        dutchRoll.SetGenericBinding(camAnimatTrack, cameraFollor);

        var clips = (bossSpawnMinonsTimeline.playableAsset as TimelineAsset).GetOutputTrack(0).GetClips();
        foreach (var c in clips)
        {
            if(c.displayName.Contains("Path"))
            {
                platformPathShot = c.asset as CinemachineShot;
            }
        }

        bossSpawnMinonsTimeline.SetReferenceValue(platformPathShot.VirtualCamera.exposedName, CMPath1);

        //bossController.activateBoss();
    }

    public void MinionKilled()
    {
        minionsKilled++;

        if(BossActiveEvents && minionsKilled == BossSpawnAmount)
        {
            if(playerDied)
            {
                minionsKilled = 0;
            }
            else
            {
                if (HasTalkedToPrincess)
                {
                    bossController.LowerShieldContinueAttacking();
                    HasTalkedToPrincess = false;
                }
                else
                {
                    bossController.ContinueMinionSpawn();
                }
                minionsKilled = 0;
            }          
        }
        else
        {
            if(minionsKilled == MiniGunSpawnAmount)
            {
                deathDialogAfterMiniOut.transform.position = playerAbilities.transform.position;
                playerAbilities.StopFiringMiniGun();
                minionsKilled = 0;
                pathBlock.SetActive(false);
            }
        }
    }

    public void StartBossSpawnMinionsTimeline(BossController.BossPhase bossPhase)
    {
        switch(bossPhase)
        {
            case BossController.BossPhase.Phase1:
                
                bossSpawnMinonsTimeline.SetGenericBinding(pathTrack, Path1);
                bossSpawnMinonsTimeline.SetReferenceValue(platformPathShot.VirtualCamera.exposedName, CMPath1);

                bossSpawnMinonsTimeline.Play();
                break;
            case BossController.BossPhase.Phase2:
                
                bossSpawnMinonsTimeline.SetGenericBinding(pathTrack, Path2);
                bossSpawnMinonsTimeline.SetReferenceValue(platformPathShot.VirtualCamera.exposedName, CMPath2);

                bossSpawnMinonsTimeline.Play();
                break;
            case BossController.BossPhase.Phase3:
                
                bossSpawnMinonsTimeline.SetGenericBinding(pathTrack, Path3);
                bossSpawnMinonsTimeline.SetReferenceValue(platformPathShot.VirtualCamera.exposedName, CMPath3);

                bossSpawnMinonsTimeline.Play();
                break;
        }

        StartCoroutine(enableControlsAfterTimeline(bossSpawnMinonsTimeline.duration));
    }

    public void EnableMiniGun()
    {
        playerAbilities.ActivateMiniGun();
        miniGunSequence.Play();

        StartCoroutine(enableControlsAndDeathAfterTimeline(miniGunSequence.duration));
    }

    public void DisableMiniGun()
    {
        playerAbilities.disableMiniGun();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !BossActiveEvents)
        {
			uIManager.ShowBossHealth ();
            BossActiveEvents = true;
            //bossController.ActivateBoss();
            BossIntro.transform.position = playerAbilities.transform.position;
        }

        if (other.tag == "Player" && BossActiveEvents && playerDied)
        {
            playerDied = false;
            bossController.PlayerDiedContine();
        }
    }

    private IEnumerator enableControlsAfterTimeline(double waitTime)
    {
        InputManager.PlayerInputsDisabled = true;
        InputManager.CameraInputsDisabled = true;

        yield return new WaitForSeconds((float)waitTime);

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;
    }

    private IEnumerator enableControlsAndDeathAfterTimeline(double waitTime)
    {
        InputManager.PlayerInputsDisabled = true;
        InputManager.CameraInputsDisabled = true;

        yield return new WaitForSeconds((float)waitTime - 2f);

        InputManager.PlayerInputsDisabled = false;
        InputManager.CameraInputsDisabled = false;

        DeathPart2.SetActive(true);
    }

    public void TalkedToPrincess()
    {
        Path1.SetActive(false);
        Path2.SetActive(false);
        Path3.SetActive(false);

        HasTalkedToPrincess = true;
    }

    public void DisablePaths()
    {
        Path1.SetActive(false);
        Path2.SetActive(false);
        Path3.SetActive(false);
    }

    public void BossKilled()
    {
        BossOutro.transform.position = playerAbilities.transform.position;
    }

    public void BossDeathSequenceOver()
    {
        DeathOutro.transform.position = playerAbilities.transform.position;
        EndGameParticals.SetActive(true);      
    }

    public void FinishedDeathOutro()
    {
        StartCoroutine(endGameWait());
    }

    private IEnumerator endGameWait()
    {
        yield return new WaitForSeconds(7f);
        gameController.GameWon();
    }

    public void ActivateDeathIntro()
    {
        DeathIntro.SetActive(true);
    }

    public void PlayDutchRollTimeline()
    {
        dutchRoll.Play();
    }
}
