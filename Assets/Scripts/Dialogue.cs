﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Dialogue
{

    //public string name;
    public bool displayOnce = false;
    public bool HasBeenDisplayed { get; set; }
	[TextArea(3,10)]
	public string[] sentences;
    public UnityEvent DialogFinished;
	//public void setName(string names) {
	//	name = names;
	//}


    public Dialogue()
    {
        HasBeenDisplayed = false;
    }
}
