﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To store abilities local to the player, such as blink and bone throwing?
public class PlayerAbilities : MonoBehaviour
{
    // Called from RewindObjectsTime
    public bool TimeRewinding { get; set; }

	private GameController gameController;

    private GameObject player;
    private float height = 0;
    //private InHandScript handScript;

    [Header("Blink")]
    public Transform playerModel;
    public Transform playerFoot;
    public Transform playerChest;
    public float blinkDistance = 5.0f;
    public float blinkCooldown = 2f;
    public GameObject blinkParticles;
    private bool blinkReady = true;

    [Header("Rewind Time")]
    public float timeRewindAmount = 2f;
    public float timeRewindCoolDown = 2f;   
    private bool timeRewindReady = true;
    public float TimeRewindTimeLeft { get; set; }
    private Coroutine timeCountDownCoroutine;
    private Coroutine timeCountUpCoroutine;
    public GameObject timeRewindParticles;
    private GameObject spawnedTimeRewindParticles;

    [Header("Bonerang")]
	private GameObject bonerang;
	public GameObject bone;
	public GameObject spawner;

	[Header("Scythe")]
	public GameObject scythe;
	public float scytheCooldown = 2f;
    //private bool scytheReady = true;

    [Header("MiniGun")]
    public GameObject miniGun;
    public PlayerIK playerIK;
    public float timeToPickupMinigun;
    public float timeToDropMinigun;
    public GameObject minigunSpawnSparticles;
    private UIManager uIManager;
    private InputManager inputManager;

    [Header("Magic Wand")]
    public TorchScript torch;
    public GameObject wandProjectile;
	private bool releaseReady = false;

    private bool wandReady = true;
	private GameObject magicNuke;

    // Use this for initialization
    void Start ()
    {
		gameController = GameObject.Find ("GameController").GetComponent<GameController> ();
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController");
        uIManager = gameControllerObj.GetComponent<UIManager>();
        inputManager = gameControllerObj.GetComponent<InputManager>();

        TimeRewindTimeLeft = 0;
        timeCountUpCoroutine = StartCoroutine(TimeCountUp());
        player = gameObject;  
		spawner = GameObject.Find ("BoneSpawn");

        
		//scythe.SetActive (false);
        //handScript = gameObject.GetComponent<InHandScript>();

        
    }
	
	// Update is called once per frame
	void Update ()
    {
        height = player.transform.position.y;
    }

    public void ActivateMiniGun()
    {
        Instantiate(minigunSpawnSparticles, miniGun.transform.position, miniGun.transform.rotation);
        AnimationManager.AnimateMinigunHoldingWithoutIK(true);
        miniGun.SetActive(true);
		gameController.SetMiniGun ();
        inputManager.MiniGunActive = true;
        StartCoroutine(playerIK.StartRightHandIK(timeToPickupMinigun));
        StartCoroutine(playerIK.StartLeftHandIK(timeToPickupMinigun));

    }

    public void disableMiniGun()
    {
        Instantiate(minigunSpawnSparticles, miniGun.transform.position, miniGun.transform.rotation);
        AnimationManager.AnimateMinigunHoldingWithoutIK(false);
        miniGun.SetActive(false);
		gameController.SetBonerang ();
        inputManager.MiniGunActive = false;
        StartCoroutine(playerIK.StopRightHandIK(timeToDropMinigun));
        StartCoroutine(playerIK.StopLeftHandIK(timeToDropMinigun));

    }

    public void StartFiringMiniGun()
    {
        miniGun.GetComponent<MiniGunControl>().StartFiring();
    }

    public void StopFiringMiniGun()
    {
        miniGun.GetComponent<MiniGunControl>().StopFiring();
    }

	public void ShootWand() {
		if (!pauseMenu.gameIsPaused) {
			if (wandReady) {
				wandReady = false;
				releaseReady = true;
				//wandReady = false;
				//StartCoroutine (wandCooldown ());
				magicNuke = Instantiate (wandProjectile, spawner.transform.position, Quaternion.identity) as GameObject;
				magicNuke.GetComponent<ParticleSystem> ().Stop ();
				//projectileClone.GetComponent<MagicWandControl> ().Shoot ();
			} else {
				return;
			}
				//projectileClone.GetComponent<MeshRenderer>().enabled = false;

		}
	}

    public void GivePlayerWand() {
        torch.GiveTorch();
    }

    public void RemovePlayerWand() {
        torch.RemoveTorch();
    }

    public void ReleaseWand() {
		if (!pauseMenu.gameIsPaused) {
			if (wandReady == false && releaseReady == true) {
				releaseReady = false;
				StartCoroutine (wandCooldown ());
				AnimationManager.AnimateBoneAttack (true);
				if (magicNuke != null) {
					magicNuke.GetComponent<MagicWandControl> ().SetRelease ();
				}
			}

		}
	}
		

    //Teleports the player in the direction he is facing 
    public void Blink()
    {
		if (!pauseMenu.gameIsPaused)
        {
            
            if(!blinkReady)
            {              
                return;
            }       
            StartCoroutine(BlinkCoolDown());

            DebugManager.Instance.IncrementBlink();

            RaycastHit hit;
            Vector3 destination = new Vector3(playerModel.transform.position.x, playerFoot.transform.position.y, playerModel.transform.position.z) + playerModel.transform.forward * blinkDistance;

            Vector3 teleportingPosition = new Vector3(playerModel.transform.position.x, playerFoot.transform.position.y, playerModel.transform.position.z);

            if (Physics.Linecast(teleportingPosition, destination, out hit))
            {
                destination = new Vector3(playerModel.transform.position.x, playerFoot.transform.position.y, playerModel.transform.position.z) + playerModel.transform.forward * (hit.distance - 1f);
            } //0.4

            if (Physics.Raycast(destination, -Vector3.up, out hit))
            {
                destination = hit.point;
                destination.y = height;
                transform.position = destination;

                Instantiate(blinkParticles, playerChest.position, Quaternion.identity);
            }
        }
	}

    public void StartTimeRewind()
    {
		if (!pauseMenu.gameIsPaused)
        {
            if (!timeRewindReady)
            {
                return;
            }
            foreach (Renderer playerRenderer in playerModel.GetComponentsInChildren<Renderer>()) {
                playerRenderer.enabled = false;
            }
            spawnedTimeRewindParticles = Instantiate(timeRewindParticles, playerChest);
            TimeRewinding = true;
            timeCountDownCoroutine = StartCoroutine(TimeCountDown());
		}
    }

    public void StopTimeRewind()
    {

       if (!TimeRewinding)
            return;

       DebugManager.Instance.IncrementRewindTime();
        uIManager.RewindTimeUICoolDownStart();
        StartCoroutine(TimeRewindCoolDown());

        foreach(Renderer playerRenderer in playerModel.GetComponentsInChildren<Renderer>()) {
            playerRenderer.enabled = true;
        }
        Destroy(spawnedTimeRewindParticles);
        TimeRewinding = false;

        timeCountUpCoroutine = StartCoroutine(TimeCountUp());
    }

    // Counts the TimeRewindleft down, when 0 time rewind stops and starts counting up. 
    private IEnumerator TimeCountDown()
    {
        StopCoroutine(timeCountUpCoroutine);

        while (TimeRewindTimeLeft > 0)
        {
            TimeRewindTimeLeft -= Time.deltaTime;
            uIManager.UpdateTimeRewindIcon(TimeRewindTimeLeft);
            yield return null;          
        }

        StopTimeRewind();
    }

	private IEnumerator wandCooldown()
	{
		wandReady = false;
		yield return new WaitForSeconds (0.95f);
		wandReady = true;
	}

    // Counts TimeRewindLeft up until upto total amount of time avalable to hold.
    private IEnumerator TimeCountUp()
    {
        if(timeCountDownCoroutine != null)
            StopCoroutine(timeCountDownCoroutine);

        while (TimeRewindTimeLeft < timeRewindAmount)
        {
            TimeRewindTimeLeft += Time.deltaTime;
            uIManager.UpdateTimeRewindIcon(TimeRewindTimeLeft);
            yield return null;
        }
    }

    private IEnumerator BlinkCoolDown()
    {
        blinkReady = false;
        yield return new WaitForSeconds(blinkCooldown);
        blinkReady = true;
    }

    public void StartTimeRewindCoolDown()
    {
        StartCoroutine(TimeRewindCoolDown());
    }

    private IEnumerator TimeRewindCoolDown()
    {
        timeRewindReady = false;
        yield return new WaitForSeconds(timeRewindCoolDown);
        timeRewindReady = true;
    }

    public void StartBonerang()
    {
		if (!pauseMenu.gameIsPaused)
        {
			if (!Bonerang.isReturned)
            {
                DebugManager.Instance.IncrementBoneThrow();
                //AnimationManager.AnimateBoneAttack(true);
                bonerang = Instantiate (bone, new Vector3 (spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z), Quaternion.identity) as GameObject;
                bonerang.GetComponent<MeshRenderer>().enabled = false;
                //bonerang.transform.localScale = bonerang.transform.localScale * 0.1f;
			}
		}
	}

	public void FlyBonerang()
    {
		if (!pauseMenu.gameIsPaused)
        {
			if (!Bonerang.released)
            {
				if (bonerang != null)
                {
                    AnimationManager.AnimateBoneAttack(true);
                    bonerang.GetComponent<MeshRenderer>().enabled = true;
                    //bonerang.transform.localScale = bonerang.transform.localScale * 10f;
                    bonerang.GetComponent<Bonerang>().OnRelease();
                    //GameObject.FindGameObjectWithTag ("Bone").GetComponent<Bonerang> ().OnRelease ();
				}
            }
		}
	}

	public void StartScythe() {
		if (!pauseMenu.gameIsPaused && !GameController.miniGunBool) {
            
            //if (!scytheReady) {
            //return;
            //}
            //handScript.putInHand(scythe);
            AnimationManager.AnimateScytheAttack(true);
            DebugManager.Instance.IncrementScytheCount();
            //handScript.setCanChangeItem(false);
            //StartCoroutine(ScytheCoolDown());
            //StartCoroutine (ScytheActive ());

        }
	}

	/*private IEnumerator ScytheActive()
	{
        //scythe.SetActive (true);
        handScript.putInHand(scythe);
        handScript.setCanChangeItem(false);
        //yield return new WaitForSeconds (scytheCooldown);
		//scythe.SetActive (false);
	}

	private IEnumerator ScytheCoolDown()
	{
		//scytheReady = false;
        //AnimationManager.AnimateScytheAttack(true);
        //yield return new WaitForSeconds(scytheCooldown);
		//scytheReady = true;
	}*/
}
