﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactReceiver : MonoBehaviour {

	float mass = 3.0f;
	Vector3 impact = Vector3.zero;
	private CharacterController character;
	private GameObject gameController;
	private GameController gameScript;

    public GameObject boneDamageParticles;

	// Use this for initialization
	void Start () {
		gameController = GameObject.FindGameObjectWithTag ("GameController");
		gameScript = gameController.GetComponent<GameController> ();
		character = GetComponent<CharacterController> ();
		character.detectCollisions = true;
		
	}

	public void addImpact(Vector3 direction, float force) {
		if (!GameController.isDead) {
			direction.Normalize ();
			if (direction.y < 0) {
				direction.y = -direction.y;
			}
			impact += direction.normalized * force / mass;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (impact.magnitude > 0.2) {
			character.Move (impact * Time.deltaTime);
		}

		impact = Vector3.Lerp(impact, Vector3.zero, 4 * Time.deltaTime);

	}

	void OnTriggerEnter(Collider col) {
		if (col.tag == "Knockback" && !GameController.isDead) {
			Vector3 knockBackVector = new Vector3 (col.transform.position.x - transform.position.x, 0, col.transform.position.z - transform.position.z);
			addImpact (-knockBackVector, 75f);
			gameScript.loseHealth (1);
		} else if (col.tag == "KnockbackNoDamage" && !GameController.isDead) {
            Vector3 knockBackVector = new Vector3(col.transform.position.x - transform.position.x, 0, col.transform.position.z - transform.position.z);
            addImpact(-knockBackVector, 75f);
        }
    }

    public void SpawnDamageParticles() {
        Instantiate(boneDamageParticles, this.transform);
    }

	//void OnControllerCollisionHit(ControllerColliderHit hit) {

		//if (hit.collider.tag == "Enemy") {
		//	addImpact (-Vector3.forward, 100f);
		//}
	//}
}
