﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyingPortal : MonoBehaviour
{
    public GameObject portalPartical;
    public GameObject bossFlyingEnemy;
    public bool FlipSpawm;

    public bool selfDestroy = false;
    public bool timelineMovement = false;

    private float spawnOffset;
    private float moveToOffset;
    private float angle;
    private GameObject enemy;

    // Use this for initialization
    void Start()
    {       
        if (FlipSpawm)
        {
            spawnOffset = 2;
            moveToOffset = -4;
            angle = -90f;
        }
        else
        {
            spawnOffset = -2;
            moveToOffset = 4;
            angle = 90f;
        }
    }

    private void OnEnable()
    {
        StartCoroutine(spawnSequence());
    }

    private IEnumerator spawnSequence()
    {
        yield return new WaitForSeconds(0.5f);

        var portal = Instantiate(portalPartical, transform.position, portalPartical.transform.rotation);
        enemy = Instantiate(bossFlyingEnemy, new Vector3(transform.position.x + spawnOffset, transform.position.y - 1f, transform.position.z), Quaternion.identity);
        enemy.transform.Rotate(new Vector3(0f, angle, 0f));

        yield return new WaitForSeconds(1f);

        iTween.MoveTo(enemy, new Vector3(transform.position.x + moveToOffset, transform.position.y - 1f, transform.position.z), 5f);


        yield return new WaitForSeconds(3f);
        if(!timelineMovement)
        {
            enemy.GetComponent<EnemyFlyingBossControl>().CanMove = true;
        }
        
        yield return new WaitForSeconds(0.5f);
        iTween.ScaleTo(portal, new Vector3(0.2f, 0.2f, 0.2f), 0.5f);
        yield return new WaitForSeconds(0.5f);

        Destroy(portal);
        if(selfDestroy)
        {
            Destroy(gameObject);
        }      
    }

    private void OnDisable()
    {
        enemy.GetComponent<EnemyFlyingBossControl>().CanMove = true;
    }
}
