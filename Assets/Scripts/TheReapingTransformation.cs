﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TheReapingTransformation : MonoBehaviour
{
    public GameObject kingModel;
    public GameObject kingMesh;
    public Rigidbody kingChestRigidbody;
    public Vector3 chestEndRotation;
    public Animator kingAnimator;
    public GameObject lichModel;

    public GameObject fireParticals;
    public GameObject deathParticals;
    public GameObject deathExplostionParticals;
    //public GameObject chargePartcals;


    public PlayableDirector timeline;

    private GameObject reapingText;
    private GameController gameController;

    // Use this for initialization
    void Start()
    {
        reapingText = GameObject.FindGameObjectWithTag("GameController").GetComponent<UIManager>().ReapingText;
        kingAnimator.enabled = true;
        var track = (timeline.playableAsset as TimelineAsset).GetOutputTrack(0);
        timeline.SetGenericBinding(track, GameObject.FindGameObjectWithTag("MainCamera"));

        var pathTrack = (timeline.playableAsset as TimelineAsset).GetOutputTrack(2);
        timeline.SetGenericBinding(pathTrack, reapingText);

        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

        // Update is called once per frame
    void Update ()
    {
		//if(Input.GetKeyDown(KeyCode.B))
  //      {
  //          Debug.Log("B");
  //          StartCoroutine(deathSequence());
  //          timeline.Play();
  //          StartCoroutine(finishBossLevelLoad((float)timeline.duration));
  //          gameController.LoadBossLevelAsync();
  //      }
	}

    public void StartReapingSequence()
    {
        StartCoroutine(deathSequence());
        timeline.Play();
        StartCoroutine(finishBossLevelLoad((float)timeline.duration));
        gameController.LoadBossLevelAsync();
    }

    private IEnumerator finishBossLevelLoad(float wait)
    {
        yield return new WaitForSeconds(wait - 0.1f);
        gameController.InReapingCutscene = false;
    }

    private IEnumerator deathSequence()
    {
        //iTween.ScaleBy(kingModel, iTween.Hash("amount", new Vector3(2, 2, 2), "time", 0.25f,
        //     "looptype", iTween.LoopType.pingPong, "easetype", iTween.EaseType.linear));

        yield return new WaitForSeconds(1f);

        kingAnimator.enabled = false;

        yield return new WaitForSeconds(1f);
        //kingChestRigidbody.isKinematic = true;
        //iTween.MoveTo(kingChestRigidbody.gameObject, transform.position, 2f);
        //StartCoroutine(MoveDeadKingToThreateningPosition(2f));

        yield return new WaitForSeconds(2f);

        var deathexpvfx = Instantiate(deathExplostionParticals, kingChestRigidbody.transform.position, deathExplostionParticals.transform.rotation);
        Destroy(deathexpvfx, 3f);
        kingModel.SetActive(false);

        yield return new WaitForSeconds(0.5f);

        var deathvfx = Instantiate(deathParticals, kingChestRigidbody.transform.position, deathParticals.transform.rotation);
        Destroy(deathvfx, 3f);
        deathvfx.transform.position = new Vector3(deathvfx.transform.position.x, deathvfx.transform.position.y + 1, deathvfx.transform.position.z);
        yield return new WaitForSeconds(1f);

        iTween.MoveTo(deathvfx, transform.position, 3f);

        yield return new WaitForSeconds(0.5f);

        var firevfx = Instantiate(fireParticals, lichModel.transform.position, fireParticals.transform.rotation);
        Destroy(firevfx, 7f);

        yield return new WaitForSeconds(2f);

        lichModel.SetActive(true);

        iTween.ScaleTo(lichModel, iTween.Hash("scale", new Vector3(3.5f, 3.5f, 3.5f), "time", 3f,
             "looptype", iTween.LoopType.none, "easetype", iTween.EaseType.linear));


        //iTween.MoveTo(objectToMove, iTween.Hash("path", patrolWayPoints.ToArray(), "speed", movementSpeed,
        //   "orienttopath", true, "looktime", .6, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop));
    }

    private IEnumerator MoveDeadKingToThreateningPosition(float time) {
        Quaternion fromAngle = kingChestRigidbody.transform.rotation;
        Vector3 fromPos = kingChestRigidbody.transform.position;


        for (float t = 0f; t < 1; t += Time.deltaTime / time) {
            kingChestRigidbody.transform.position = Vector3.Lerp(fromPos, transform.position, Mathf.SmoothStep(0f, 1f, t));
            kingChestRigidbody.transform.rotation = Quaternion.Lerp(fromAngle, Quaternion.Euler(chestEndRotation.x, chestEndRotation.y, chestEndRotation.z), Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }
}
