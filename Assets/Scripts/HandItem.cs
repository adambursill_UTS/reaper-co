﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface HandItem {
    GameObject spawnParticles { get; }
    GameObject despawnParticles { get; }

    void OnSpawn();

    void OnDespawn();
}
