﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class pauseMenu : MonoBehaviour {

    // Use this for initialization
    public static bool gameIsPaused = false;
	public GameObject pauseMenuUI;
	public Text warningText;
	public GameObject restartButton;
	public GameObject quitButton;
	public GameObject resumeButton;
	private EventSystem eventSystem;

	private GameObject bone;
	private GameObject scythe;

	//private bool canInteract = true;

	void Start ()
    {
        gameIsPaused = false;
        eventSystem = EventSystem.current;
		eventSystem.SetSelectedGameObject (resumeButton, new BaseEventData (eventSystem));
        LockMouse(true);
        //Cursor.lockState = CursorLockMode.Locked;
    }

    public void PauseOrResumeGame()
    {
        if (gameIsPaused)
        {
			Resume ();
        }
        else
        {
            Pause();
        }

        // so mouse only shows up when keyboard is used
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            LockMouse(false);
        }
    }

	public void Resume() {
		pauseMenuUI.SetActive (false);
		gameIsPaused = false;
		Time.timeScale = 1f;
		warningText.text = "";
		restartButton.SetActive (false);
		quitButton.SetActive (false);
        LockMouse(true);
        //Cursor.lockState = CursorLockMode.Locked;
    }

	void Pause() {
		bone = GameObject.FindGameObjectWithTag ("Bone");
		if (bone != null) {
			bone.GetComponent<AudioSource> ().Stop ();
		}
		scythe = GameObject.Find ("ScytheAttack");
		if (scythe != null) {
			scythe.GetComponent<AudioSource> ().Stop ();
		}
		pauseMenuUI.SetActive (true);
		gameIsPaused = true;
		Time.timeScale = 0f;
        //LockMouse(false);
        //Cursor.lockState = CursorLockMode.None;
    }

	public void LoadWarning() {
		warningText.text = "Are you sure? Clicking yes will restart the game";
		quitButton.SetActive (false);
		restartButton.SetActive (true);
	}

	//public void LoadMenu() {
	//	Time.timeScale = 1f;
	//	gameIsPaused = false;
	//	Bonerang.isReturned = false;
	//	SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
 //   }
	public void QuitWarning() {
		warningText.text = "Are you sure? Clicking yes will quit the game";
		restartButton.SetActive (false);
		quitButton.SetActive (true);
	}

	public void QuitGame() {
		Debug.Log ("Quit Game");
        SceneManager.LoadScene("Main Menu");
        Time.timeScale = 1f;
    }

    private void LockMouse(bool locked) {
        if (locked) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        } else {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

}
