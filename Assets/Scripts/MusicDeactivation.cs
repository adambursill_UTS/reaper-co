﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicDeactivation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.Find ("Main Camera").GetComponent<AudioSource> ().Stop ();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
