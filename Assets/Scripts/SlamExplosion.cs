﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlamExplosion : MonoBehaviour
{
    public GameObject explosion;
    public GameObject shockWave;


	// Use this for initialization
	void Start ()
    {
        var explosionG = Instantiate(explosion, gameObject.transform.position, explosion.transform.rotation);

        var shockW = Instantiate(shockWave, gameObject.transform.position, explosion.transform.rotation);

        iTween.ScaleTo(shockW, new Vector3(80, 85, 80), 5f);

        Destroy(explosionG, 2f);
        Destroy(shockW, 5f);
    }
	
}
