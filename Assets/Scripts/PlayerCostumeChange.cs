﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCostumeChange : MonoBehaviour {

    public GameObject[] defaultSkeletonObjects;
    public GameObject[] dressObjects;
    public GameObject princessHat;

    public enum Costume { Default, PrincessDress }
    public Costume playerCostume = Costume.Default;

    private bool wearingPrincessHat;

    // Use this for initialization
    void Start (){
        wearingPrincessHat = false;
        CostumeChange();
    }


    public void CostumeChange() {
        if (playerCostume == Costume.PrincessDress) {
            foreach (GameObject dressObject in dressObjects) {
                dressObject.SetActive(true);
            }
            foreach (GameObject defaultObject in defaultSkeletonObjects) {
                defaultObject.SetActive(false);
            }
        } else if(playerCostume == Costume.Default) {
            foreach (GameObject dressObject in dressObjects) {
                dressObject.SetActive(false);
            }
            foreach(GameObject defaultObject in defaultSkeletonObjects) {
                defaultObject.SetActive(true);
            }
        }
    }

    public void CostumeChange(Costume newCostume) {
        playerCostume = newCostume;
        CostumeChange();
    }

    public void CostumeChangeToPrincess() {
        CostumeChange(Costume.PrincessDress);
    }

    public void WearPrincessHat(bool wearingHat) {
        wearingPrincessHat = wearingHat;
        princessHat.SetActive(wearingHat);
    }

    public List<GameObject> GetCurrentCostume() {
        List<GameObject> costumeParts = new List<GameObject>();
        
        if (playerCostume == Costume.PrincessDress) {
            costumeParts.AddRange(dressObjects);
        } else if(playerCostume == Costume.Default) {
            costumeParts.AddRange(defaultSkeletonObjects);
        }
        if (wearingPrincessHat) {
            costumeParts.Add(princessHat);
        }
        return costumeParts;
    }
}
