﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DebugToFile {
    public static string filePath = "./";

    public static void Log(string message, string filename) {
        string destination = filePath + filename ;
        File.WriteAllText(destination, message);
    }
}
