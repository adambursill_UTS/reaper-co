﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{

    public string Name;
    public bool TriggerAutomatically = false;
    public bool TurnToPlayerWhenSpeaking = false;

    public List<Dialogue> dialogues;

    private bool isClose = false;

    private UIManager uIManager;
    private InputManager inputManager;

    private int dialogueCount = 0;

    private bool hasRun = false;

    private Animator animator;
    private GameObject player;
    private Vector3 originalLook;

    void Start()
    { 
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
		uIManager = gameControllerObject.GetComponent<UIManager>();
		inputManager = gameControllerObject.GetComponent<InputManager>();
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        originalLook = transform.forward * 20 + transform.position;
    }

	void Update()
    {
		if (isClose)
        {
			if (inputManager.Interact && !UIManager.inDialogue)
			{
				TriggerDialogue ();
                TalkingAnimation(true);
                if(TurnToPlayerWhenSpeaking)
                {
                    Vector3 lookAtPos = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                    iTween.LookTo(gameObject, lookAtPos, 1.5f);
                }              
            }		
		}
	}

    public void ReturnToOriginalLook()
    {
        if (TurnToPlayerWhenSpeaking)
        {
            iTween.LookTo(gameObject, originalLook, 1.5f);
        }       
    }

    public void ResetDialogueCount() {
        dialogueCount = 0;
    }




    public void TriggerDialogue()
    {
        bool didDisplayerDialog = false;

        // Handles when a dialog is only to be displayed once
        if(!dialogues[dialogueCount].HasBeenDisplayed)
        {
            FindObjectOfType<DialogueManager>().StartDialogue(dialogues[dialogueCount], Name, this);
            didDisplayerDialog = true;

            // When a dialog is to only be displayed once, set it to not be redisplayed
            if (dialogues[dialogueCount].displayOnce)
            {
                dialogues[dialogueCount].HasBeenDisplayed = true;
            }
        }
      		
        if(dialogueCount < dialogues.Count - 1)
        {
            dialogueCount++;
        }
        else
        {
            dialogueCount = 0;
        }

        // If we did not display dialog try again now we have moved along the list
        if(!didDisplayerDialog)
        {
            TriggerDialogue();
        }
	}

	private void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player" && !TriggerAutomatically)
		{
            uIManager.ShowHintPanel();
			isClose = true;
		}

		if (other.tag == "Player" && TriggerAutomatically && !hasRun)
        {
            hasRun = true;
			TriggerDialogue ();         
		}
	}

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        { 
            uIManager.HideHintPanel();
            isClose = false;
            hasRun = false;
        }      
	}

    public void TalkingAnimation(bool isTalking) {
        if (animator != null) {
            animator.SetBool("Talking", isTalking);
            /*foreach (AnimatorControllerParameter param in animator.parameters) {
                if (param.name == "Talking") {
                    animator.SetBool(Talk)
                }

            }*/
        }
    }
}
