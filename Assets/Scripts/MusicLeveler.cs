﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicLeveler : MonoBehaviour {

	bool isLeveled = true;
	bool isLeveling = false;

	public AudioClip HubWorld;
	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (isLeveled == false && GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume < 0.499f && isLeveling == false) {
			isLeveling = true;
			GameObject.Find ("Main Camera").GetComponent<AudioSource> ().clip = HubWorld;
			GameObject.Find ("Main Camera").GetComponent<AudioSource> ().Play ();
		}

		if (GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume >= 0.499f) {
			isLeveling = false;
			isLeveled = true;
		}

		if (isLeveling == true) {
			GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume += 0.01f;
		}
	}
		
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			if (GameObject.Find ("Main Camera").GetComponent<AudioSource> ().volume <= 0.499f) {
				isLeveled = false;
			}
		}
	}

}
