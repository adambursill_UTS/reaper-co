﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class Hud : MonoBehaviour {
    
    public Inventory inventory;

    // Use this for initialization
    void Start () {
        inventory.ItemAdded += InventoryScript_ItemAdded;
        inventory.ItemRemoved += InventoryScript_ItemRemoved;
    }

    private void InventoryScript_ItemAdded(object sender, InventoryEventArgs e)
    {
        Transform inventoryPanel = transform.Find("InventoryPanel");
        foreach(Transform slot in inventoryPanel)
        {
            Transform imageTransform = slot.GetChild(0).GetChild(0);
            Transform borderTransform = slot.GetChild(0);
            Image image = imageTransform.GetComponent<Image>();
            OnSelect select = borderTransform.GetComponent<OnSelect>();

            if (!image.enabled)
            {
                image.enabled = true;
                image.sprite = e.Item.Image;

                select.Item = e.Item;

                //store reference to item

                break;
            }
        }
    }

    private void InventoryScript_ItemRemoved(object sender, InventoryEventArgs e)
    {
        Transform inventoryPanel = transform.Find("InventoryPanel");
        foreach (Transform slot in inventoryPanel)
        {
            Transform imageTransform = slot.GetChild(0).GetChild(0);
            Transform borderTransform = slot.GetChild(0);
            Image image = imageTransform.GetComponent<Image>();
            OnSelect select = borderTransform.GetComponent<OnSelect>();

            //if (select.selectedItem.ToString() == e.Item.ToString())
            //{
            //    image.enabled = false;
            //    image.sprite = null;

            //    select.Item = null;

            //    break;
            //}
            
            if (select.Item != null && select.Item.Name == e.Item.Name)
            {
                image.enabled = false;
                image.sprite = null;

                select.Item = null;

                break;
            } 
        }
    }
}
