﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InventoryItem : MonoBehaviour, IInventoryItem
{
    public string _name;
    public string Name
    {
        get
        {
            return _name;
        }
    }

    public Sprite _Image = null;

    public Sprite Image
    {
        get
        {
            return _Image;
        }
    }

    public UnityEvent OnPickupEvent;
    public UnityEvent OnUseEvent;

    public void OnPickup()
    {
        gameObject.SetActive(false);
        OnPickupEvent.Invoke();
    }

    public void OnUse()
    {
        OnUseEvent.Invoke();
    }
}
