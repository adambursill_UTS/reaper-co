﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupOutline : MonoBehaviour {

    //public GameObject pickup;
    private Shader outlineShader;
    private Shader standard;

	// Use this for initialization
	void Start () {

        outlineShader = Shader.Find("Outlined/Custom");
        standard = Shader.Find("Standard");
	}

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.tag == "Pickup" || gameObject.tag == "Useable")
        {
            GetComponent<Renderer>().material.shader = outlineShader;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (gameObject.tag == "Pickup" || gameObject.tag == "Useable")
        {
            GetComponent<Renderer>().material.shader = standard;
        }
    }

}
