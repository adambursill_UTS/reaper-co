﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {
    
    public Hud hud;
    public InputManager input;
    
    public List<IInventoryItem> mItems = new List<IInventoryItem>();

    public event EventHandler<InventoryEventArgs> ItemAdded;
    public event EventHandler<InventoryEventArgs> ItemRemoved;

    private const int inventorySlots = 6;

    private UIManager uIManager;
    private InputManager inputManager;

    private void Start()
    {        
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        uIManager = gameControllerObject.GetComponent<UIManager>();
        inputManager = gameControllerObject.GetComponent<InputManager>();
    }    

    public void ClearInventory()
    {
		try
		{
			for (int i = 0; i < mItems.Count; i++)
			{
				if (mItems[i] != null)
				{
					RemoveItem(mItems[i]);
				}
			}
		}
		catch (Exception e)
		{
			Debug.Log("Inventory clear exception" + e.Message);
		}

		uIManager.HideInventoryPanel();
    }

    public void AddItem(IInventoryItem item)
    {
        uIManager.ShowInventoryPanel();
        if (mItems.Count < inventorySlots)
        {
            Collider collider = (item as MonoBehaviour).GetComponent<Collider>();
            if(collider.enabled)
            {
                collider.enabled = false;
                mItems.Add(item);
                item.OnPickup();

                if (ItemAdded != null)
                {
                    ItemAdded(this, new InventoryEventArgs(item));
                }
            }
        }
    }

    public void RemoveItem(IInventoryItem item)
    {       
        if (mItems.Contains(item))
        {
            mItems.Remove(item);

            if (mItems.Count == 0)
            {
                uIManager.HideInventoryPanel();
            }

            item.OnUse();

            Collider collider = (item as MonoBehaviour).GetComponent<Collider>();
            if (collider != null)
            {
                collider.enabled = true;

            }
            if (ItemRemoved != null)
            {
                ItemRemoved(this, new InventoryEventArgs(item));
            }            
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Pickup")
        {
            uIManager.ShowHintPanel();
            if (inputManager.Interact)
            {
                IInventoryItem item = other.gameObject.GetComponent<IInventoryItem>();
                if (item != null)
                {
                    uIManager.HideHintPanel();
                    AddItem(item);
                }
            }
        }

        if(other.tag == "Useable")
        {
            uIManager.ShowHintPanel();
            if (inputManager.Interact)
            {
                other.gameObject.GetComponent<PickUpAble>().OnPickUp.Invoke();
                uIManager.HideHintPanel();
            }
        }

        // May need this code for a later sprint
        //if (other.tag == "Drop")
        //{            
        //    if (Input.GetButtonDown("Use") && input.inventoryOpen == true)
        //    {
        //        IInventoryItem item = other.GetComponent<Collider>().GetComponent<IInventoryItem>();

        //        GameObject canvas = GameObject.Find("HudCanvas");
        //        Transform inventoryPanel = canvas.GetComponentInChildren<Transform>().transform.Find("InventoryPanel");
        //        foreach (Transform slot in inventoryPanel)
        //        {
        //            Transform borderTransform = slot.GetChild(0);
        //            OnSelect select = borderTransform.GetComponent<OnSelect>();


        //            if (item != null && select.selectedItem.ToString() == item.ToString())
        //            {
        //                RemoveItem(item);
        //            }
        //        }                
        //    }
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        uIManager.HideHintPanel();
    }
}
