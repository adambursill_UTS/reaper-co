﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OnSelect : MonoBehaviour, ISelectHandler, IDeselectHandler {

    public string selectedItem;

    public IInventoryItem Item { get; set; }

    public void OnDeselect(BaseEventData eventData)
    {
        selectedItem = "nothing";
    }

    void ISelectHandler.OnSelect(BaseEventData eventData)
    {
        if (Item != null)
        {
            selectedItem = Item.Name.ToString() + " (" + Item.Name.ToString() + ")";
            Debug.Log(selectedItem);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
