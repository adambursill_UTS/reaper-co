﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoneStopFireworks : MonoBehaviour
{

    public GameObject fireWorksParticals;

	// Use this for initialization
	void Start ()
    {
		
	}


    private void OnEnable()
    {
        StartCoroutine(continuousFireworks());
    }

    private IEnumerator continuousFireworks()
    {
        while(true)
        {
            var VFX = Instantiate(fireWorksParticals, transform.position, fireWorksParticals.transform.rotation);
            VFX.transform.localScale = new Vector3(6f, 6f, 6f);

            Destroy(VFX, 4f);
            yield return new WaitForSeconds(2f);
        }
    }
}
