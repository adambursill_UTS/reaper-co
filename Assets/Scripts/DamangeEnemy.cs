﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamangeEnemy : MonoBehaviour {

	public GameObject deathSounds;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
			Instantiate (deathSounds, transform.position, Quaternion.identity);
            other.gameObject.GetComponent<EnemyBehaviour>().Die();
        }

        if (other.tag == "BossEnemy")
        {
            other.gameObject.GetComponent<EnemyGroundBossControl>().Die();
        }
    }
}
