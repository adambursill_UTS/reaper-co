﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MusicFader : MonoBehaviour {
	public UnityEvent onActive;

	private bool leverOn = false;
	private UIManager uIManager;

	public GameObject musicChanger;

	private InputManager inputManager;
	// Use this for initialization
	void Start () {
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
		uIManager = gameControllerObject.GetComponent<UIManager>();
		inputManager = gameControllerObject.GetComponent<InputManager>();
	}
	
	void Update () {

		if (leverOn == true)
		{
			if (inputManager.Interact)
			{
				musicChanger.SetActive (true);
			}
					
		}
			

	}

	private void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player")
		{
			uIManager.ShowHintPanel();
			leverOn = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		uIManager.HideHintPanel();
		leverOn = false;
	}
		
}
