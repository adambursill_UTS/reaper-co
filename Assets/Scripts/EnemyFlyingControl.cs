﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyingControl : MonoBehaviour
{

    public float PatrolWidth;
    public float PatrolLength;
    public float movementSpeed;

    private List<Vector3> patrolWayPoints;

    private GameObject objectToMove;
    // Use this for initialization
    void Start ()
    {
        objectToMove = transform.GetChild(0).gameObject;
        generateWayPoints();
        MoveToStartPosition();
        StartMove();     
    }
	

    private void StartMove()
    {
        iTween.MoveTo(objectToMove, iTween.Hash("path", patrolWayPoints.ToArray(), "speed", movementSpeed, 
            "orienttopath", true, "looktime", .6, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop));
    }

    public void MoveToStartPosition()
    {        
        transform.GetChild(0).position = patrolWayPoints[0];
    }


    void OnDrawGizmosSelected()
    {
        generateWayPoints();

        iTween.DrawPath(patrolWayPoints.ToArray(), Color.blue);
    }

    private void generateWayPoints()
    {
        patrolWayPoints = new List<Vector3>
        {
            new Vector3(transform.position.x + PatrolWidth / 2, transform.position.y, transform.position.z - PatrolLength / 2),
            new Vector3(transform.position.x + PatrolWidth / 2, transform.position.y, transform.position.z + PatrolLength / 2),
            new Vector3(transform.position.x - PatrolWidth / 2, transform.position.y, transform.position.z + PatrolLength / 2),
            new Vector3(transform.position.x - PatrolWidth / 2, transform.position.y, transform.position.z - PatrolLength / 2),
            new Vector3(transform.position.x + PatrolWidth / 2, transform.position.y, transform.position.z - PatrolLength / 2)
        };
    }

    public void HasDied()
    {
        // Used to notify when a flying enemy has been killed ie count
        Destroy(gameObject);
    }
}
