﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// This component will change the dialog in Dialogue trigger if its method is called or if you player
// has the item matching itemRequired.
[RequireComponent(typeof(DialogueTrigger))]
public class DialogueChangeListener : MonoBehaviour
{
    public string uniqueID;
    public bool itemIsRequired;

    // Name must match name given to the item
    public string itemRequiredName;


    public List<Dialogue> dialogues;

    private DialogueTrigger dialogueTrigger;
    private Inventory playerInventory;

    // Use this for initialization
    void Start ()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
        playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
	}

    // This is to be called from anything that wants to change this npc's dialog
    public void ChangeDialog()
    {
        dialogueTrigger.dialogues = this.dialogues;
        dialogueTrigger.ResetDialogueCount();
    }

    // This should be called from a dialog finished event
    public void RemoveItemFromInvetory()
    {
        playerInventory.RemoveItem(playerInventory.mItems.FirstOrDefault(x => x.Name == itemRequiredName));       
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && itemIsRequired)
        {
            if (playerInventory.mItems.Any(x => x.Name == itemRequiredName))
            {              
                 ChangeDialog();                            
            }
        }
    }
}
