﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TriggerBirdeyeTimeline : MonoBehaviour {

    private PlayableDirector timeline;
    private BossEvents bossEvents;

    // Use this for initialization
    void Start ()
    {
        timeline = GetComponent<PlayableDirector>();
        bossEvents = GameObject.Find("BossEvents").GetComponent<BossEvents>();

        var cameraTrack = (timeline.playableAsset as TimelineAsset).GetOutputTrack(0);
        timeline.SetGenericBinding(cameraTrack, GameObject.FindGameObjectWithTag("MainCamera"));

    }
	

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && bossEvents.HasTalkedToPrincess)
        {
            timeline.Play();
        }
    }
}
