﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemyFlyingControl))]
public class EnemyFlyingControlEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EnemyFlyingControl enemyFlyingControl = (EnemyFlyingControl)target;

        if (GUILayout.Button("Move To Start"))
        {
            enemyFlyingControl.MoveToStartPosition();
        }
    }
}
