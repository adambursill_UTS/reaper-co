﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePlatform : MonoBehaviour {

    public Animator bluePlatform;
    private bool onPlatform;

	// Use this for initialization
	void Start () {
        bluePlatform.SetBool("drop", false);
	}
	
	// Update is called once per frame
	void Update () {
		
        if (onPlatform == true)
        {
            bluePlatform.SetBool("drop", true);
            Invoke("ResetPlatform", 1);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            onPlatform = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        onPlatform = false;
    }

    private void ResetPlatform()
    {
        bluePlatform.SetBool("drop", false);
        onPlatform = false;
    }

}
